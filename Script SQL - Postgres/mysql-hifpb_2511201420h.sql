--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2015-02-20 08:00:36 BRT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 231 (class 3079 OID 12018)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2565 (class 0 OID 0)
-- Dependencies: 231
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 244 (class 1255 OID 26896)
-- Name: add_aula(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_aula() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare r record;
   declare nome varchar;
   declare cod integer;
   declare per integer;
   declare disc integer;
   declare nova_turma record;
   declare cod_turma_novo integer;
   declare cod_turma_atual integer;
begin
   -- não podem existir duas aulas da mesma turma no mesmo horário
   -- periodo da disciplina da nova turma
   select into per d.periodo from turma t, disciplina d where t.disciplina = d.codigo and t.codigo = new.turma;
   -- verificando
   select into r d.abreviacao as disciplina, t.identificacao as turma
   from aula a, turma t, disciplina d, semestre s
   where a.turma = t.codigo and t.disciplina = d.codigo and t.codigo = new.turma and a.horario = new.horario and 
         a.dia = new.dia and d.periodo = per and t.semestre = s.codigo and s.ativo = true;
   if r is not null then
      raise exception 'Turma [%/%/%], já está sendo oferecida nesse horário.', per, r.disciplina, r.turma;
   end if;

   -- um professor não pode estar em dois lugares (aulas) ao mesmo tempo.
   -- codigo do novo professor
   select into cod p.codigo from professor p, turma t where t.professor = p.codigo and t.codigo = new.turma;
   -- verificando
   select into r p.abreviacao as abvProfessor, c.abreviacao as abvCurso, d.periodo as periodo, d.abreviacao as abvDisciplina, t.identificacao as identificacao, t.codigo as codturma
   from aula a, turma t, professor p, disciplina d, curso c, semestre s
   where a.turma = t.codigo and t.professor = p.codigo and t.disciplina = d.codigo and d.curso = c.codigo and -- junções
         a.horario = new.horario and a.dia = new.dia and p.codigo = cod and t.semestre = s.codigo and s.ativo = true;

   if (r is not null) then
        -- obtendo a identificacao da turma que está sendo adicionada
       select into nova_turma codigo, identificacao, disciplina, semestre, professor, disciplina_turma_compartilhada from turma where codigo = new.turma; --253

       if ((nova_turma.identificacao = 'X') and (r.identificacao = 'X')) then
          -- verificar se é a mesma turma compartilhada
	  select into cod_turma_novo dtc.turma_compartilhada from turma t, disciplina_turma_compartilhada dtc where t.disciplina_turma_compartilhada = dtc.codigo and t.codigo = nova_turma.codigo;
	  select into cod_turma_atual dtc.turma_compartilhada from turma t, disciplina_turma_compartilhada dtc where t.disciplina_turma_compartilhada = dtc.codigo and t.codigo = r.codturma;
	  if (cod_turma_novo <> cod_turma_atual) then
	     raise exception 'Professor [%] está registrado na disciplina [%/%/%] nesse horário.', r.abvProfessor, r.abvCurso, r.periodo, r.abvDisciplina;
          end if;          
       else
	    raise exception 'Professor [%] está registrado na disciplina [%/%/%] nesse horário.', r.abvProfessor, r.abvCurso, r.periodo, r.abvDisciplina;
	end if;
   end if;

   -- não podem existir duas aulas no mesmo ambiente no mesmo horário [verificando sala]
   if new.sala is not null then
	select into r * from aula where new.horario = horario and new.dia = dia and new.sala = sala;
	if r is not null then
	    select into nome descricao from sala s where s.codigo = new.sala;
	    raise exception 'A % já está ocupada nesse horário.', nome;
	end if;
   end if;

   -- não podem existir duas aulas no mesmo ambiente no mesmo horário [verificando laboratório]
   if new.laboratorio is not null then
      select into r * from aula where new.horario = horario and new.dia = dia and new.laboratorio = laboratorio;
      if r is not null then
         select into nome descricao from laboratorio l where l.codigo = new.laboratorio;
         raise exception 'O % já está ocupado nesse horário.', nome;
      end if;
   end if;

   
   -- não podem existir duas turmas do mesmo curso, período igual, disciplina diferente, no mesmo horário
   
   -- Nova aula: codigo do curso
   select into cod c.codigo from turma t, disciplina d, curso c where new.turma = t.codigo and t.disciplina = d.codigo and d.curso = c.codigo;
   -- Nova aula: periodo da disciplina
   select into per d.periodo from turma t, disciplina d where new.turma = t.codigo and t.disciplina = d.codigo;
   -- Nova aula: codigo da disciplina
   select into disc d.codigo from turma t, disciplina d where new.turma = t.codigo and t.disciplina = d.codigo;

   -- verificando dados obtidos
   for r in select t.codigo as codTurma, c.codigo as codCurso, d.codigo as codDisciplina, d.descricao as nomeDisciplina, 
	           d.periodo as period, a.sala as codSala, a.laboratorio as codLaboratorio
	    from aula a, turma t, disciplina d, curso c, semestre s
	    where a.turma = t.codigo and t.disciplina = d.codigo and d.curso = c.codigo and new.horario = a.horario and new.dia = a.dia and
		      t.semestre = s.codigo and s.ativo = true
   loop
      if (new.turma = r.codTurma) then
         raise exception 'Essa turma já está sendo oferecida nesse horário (duplicidade).';
      end if;
      if (r.codCurso = cod and r.period = per and r.codDisciplina != disc) then
         raise exception 'Disciplina % (mesmo período) já está sendo oferecida nesse horário.', r.nomeDisciplina;
      end if;
   end loop;

   return new;
end;
$$;


ALTER FUNCTION public.add_aula() OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 26897)
-- Name: add_aula_turma_vinculada(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_aula_turma_vinculada() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare a record;
begin
   if (new.identificacao = 'X') then
      -- obter as aulas que já foram registradas (turma compartilhada)
      for a in 	select horario, dia, sala, laboratorio
		from aula
		where turma in (
			select t.codigo
			from turma t, semestre s
			where s.ativo = true and t.semestre = s.codigo and
			      disciplina_turma_compartilhada in (
					select codigo 
					from disciplina_turma_compartilhada 
					where turma_compartilhada = (
						select turma_compartilhada 
						from disciplina_turma_compartilhada 
						where codigo = new.disciplina_turma_compartilhada
					) and codigo != new.disciplina_turma_compartilhada limit 1
				)
			)
	loop
	   -- adiciona aula para a nova turma
	   insert into aula (horario, dia, sala, laboratorio, turma) values (a.horario, a.dia, a.sala, a.laboratorio, new.codigo);
	end loop;
   end if;   
   return new;
end;
$$;


ALTER FUNCTION public.add_aula_turma_vinculada() OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 26898)
-- Name: add_disciplina_turma_vinculada(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_disciplina_turma_vinculada() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare t record;
   declare tc record;
   declare dtc record;
   declare d record;
   declare a_cadastrada record;
   declare a_nova record;
begin
   -- obtendo informações sobre a turma compartilhada
   select into t * from turma_compartilhada where codigo = new.turma_compartilhada;

   -- verificando se a disciplina já possui turma compartilhada 
   select into tc * from turma where disciplina = new.disciplina and semestre = t.semestre and identificacao like 'X';

   if (tc is not null) then
      raise exception 'Essa disciplina já possui turma compartilhada, não é possível criar outra';
   end if;

   -- verificando se a turma compartilhada já possui disciplina do mesmo período da nova disciplina a ser compartilhada
   for dtc in select tt.descricao, tdtc.disciplina, td.abreviacao, td.periodo, td.curso
	      from turma_compartilhada tt, disciplina_turma_compartilhada tdtc, semestre s, disciplina td
	      where 	s.ativo = true and 
			tt.semestre = s.codigo and
			tdtc.turma_compartilhada = tt.codigo and
			tdtc.disciplina = td.codigo and
			tt.codigo = new.turma_compartilhada
   loop
      -- disciplina que está sendo adicionada	
      select into d curso, periodo from disciplina where codigo = new.disciplina;

      if ((d.curso = dtc.curso) and (d.periodo = dtc.periodo)) then
         raise exception 'Essa turma compartilhada já possui disciplina no %º período. Não é possível vincular outra do mesmo período.', d.periodo;
      end if;
   end loop;

   -- obter as aulas de uma turma compartilhada
   for a_cadastrada in select codigo, horario, dia
		       from aula
		       where turma in (select codigo from turma where disciplina_turma_compartilhada in (select codigo from disciplina_turma_compartilhada where turma_compartilhada = new.turma_compartilhada) limit 1)
   loop
      -- verificar se no curso dessa nova disciplina já tem aula alocada nesse dia/horário da aula
      for a_nova in select codigo, horario, dia
		    from aula
		    where turma in (select tu.codigo from turma tu, semestre s where s.ativo = true and tu.semestre = s.codigo and  tu.disciplina in (
				select codigo from disciplina where curso = (select c.codigo from curso c, disciplina di where di.curso = c.codigo and di.codigo = new.disciplina)))
      loop
	 if ((a_cadastrada.horario = a_nova.horario) and (a_cadastrada.dia = a_nova.dia)) then
	    raise exception 'Não é possível vincular a disciplina. O horário da(s) aula(s) da turma compartilhada está alocado para outra disciplina.';
	 end if;
      end loop;      
   end loop;
      
   return new;
end;
$$;


ALTER FUNCTION public.add_disciplina_turma_vinculada() OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 26899)
-- Name: add_disponibilidade_professor(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_disponibilidade_professor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  declare cod_semestre integer;
begin
   -- obtém semestre ativo
   select into cod_semestre codigo from semestre where ativo = true;

   -- adiciona disponibilidade
   insert into disponibilidade (professor, semestre, disp_segunda, disp_terca, disp_quarta, disp_quinta, disp_sexta, restricao) values (new.codigo, cod_semestre, true, true, true, true, true, 'Definida no Cadastro');

   return new;
end;
$$;


ALTER FUNCTION public.add_disponibilidade_professor() OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 26900)
-- Name: add_professor(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_professor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  declare s record;
begin

   -- verifica se existe semestre ativo (para posteriormente adicionar disponibilidade)
   select into s * from semestre where ativo = true;
   if s is null then
	raise exception 'Não é possível adicionar professor sem antes configurar o semestre ativo.';
   end if;

   new.ativo = true;
   return new;
end;
$$;


ALTER FUNCTION public.add_professor() OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 26901)
-- Name: add_turma(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_turma() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare qtde integer;
begin
   new.identificacao = upper(new.identificacao);

   -- verificando se existe uma turma já cadastrada com esses dados
   select into qtde count(codigo) from turma where disciplina = new.disciplina and semestre = new.semestre and identificacao = new.identificacao;

   if (qtde > 0) then
      raise exception 'Para a disciplina selecionada, já existe a turma com identificação "%" registrada no sistema.', new.identificacao;
   end if;
   
   -- obtendo a quantidade de turmas para essa disciplina e semestre.
   select into qtde count(codigo) from turma where disciplina = new.disciplina and semestre = new.semestre;

   if (new.identificacao = 'U') then
      if (qtde > 0) then
	 raise exception 'Essa turma já foi dividida, não é possível criar turma única.';
      end if;
   else
      -- verificar se existe turma 'U'
      select into qtde count(codigo) from turma where disciplina = new.disciplina and semestre = new.semestre and identificacao like 'U';
      if (qtde > 0) then
	raise exception 'Não é possível dividir a turma, já existe uma turma única cadastrada.';
      end if;

      -- se a nova turma for 'A', buscar turma 'X', caso exista não pode cadastrar.
      if (new.identificacao = 'A') then
         select into qtde count(codigo) from turma where disciplina = new.disciplina and semestre = new.semestre and identificacao like 'X';
         if (qtde > 0) then
            raise exception 'Não é possível cadastrar turma A, já existe uma turma X (compartilhada) cadastrada.';
         end if;
      else -- se a nova turma for 'X', buscar turma 'A', caso exista não pode cadastrar.
          if (new.identificacao = 'X') then
             select into qtde count(codigo) from turma where disciplina = new.disciplina and semestre = new.semestre and identificacao like 'A';
             if (qtde > 0) then
                raise exception 'Não é possível cadastrar turma X, já existe uma turma A (dividida) cadastrada.';
             end if;
          end if;
      end if;      
   end if;
   
   return new;
end;
$$;


ALTER FUNCTION public.add_turma() OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 26902)
-- Name: addaulasturmascompartilhadas(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION addaulasturmascompartilhadas(p_disciplina integer, p_dia integer, p_horario integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
   declare r record;
   declare qtde integer;
begin
   -- obtendo todas as turmas que deve ser verificado e adicionado
   for r in select t.codigo, t.identificacao, t.disciplina, t.semestre, t.professor, c.codigo as codcurso, d.periodo as periodo, c.abreviacao as abrevcurso
   from turma t, disciplina d, curso c, semestre s
   where s.ativo = true and t.semestre = s.codigo and
         t.disciplina = d.codigo and
         d.curso = c.codigo and
         t.identificacao = 'X' and
               d.codigo in (
               		      select dtc.disciplina
               		      from turma_compartilhada tc, disciplina_turma_compartilhada dtc, semestre s
               		      where s.ativo = true and 
               		    	    tc.semestre = s.codigo and
               		    	    dtc.turma_compartilhada = tc.codigo and
               		    	    tc.codigo = (select dtc.turma_compartilhada from disciplina_turma_compartilhada dtc where dtc.disciplina = p_disciplina)
               		   )

   loop
	-- raise notice 'Verificando: Curso % - Disciplina % - Periodo %', r.codcurso, r.disciplina, r.periodo;
       -- verificando se é possível adicionar todas as aulas (de todas as turmas)
       select into qtde count(*) from aulas where codcurso = r.codcurso and periodo = r.periodo and codhorario = p_horario and coddia = p_dia;
       if (qtde > 0) then
	   raise exception 'Não é possível registrar a aula compartilhada. Para esse dia e horário já existe aula registra no curso %, período %.', r.abrevcurso, r.periodo;
       end if;
   end loop;

   -- pode adicionar as aulas com sucesso (exceto da disciplina que está provocando o registro da aula)
   for r in select t.codigo, t.identificacao, t.disciplina, t.semestre, t.professor, c.codigo as codcurso, d.periodo as periodo
   from turma t, disciplina d, curso c, semestre s
   where s.ativo = true and t.semestre = s.codigo and
         t.disciplina = d.codigo and
         d.curso = c.codigo and
         t.identificacao = 'X' and
               d.codigo in (
               		      select dtc.disciplina
               		      from turma_compartilhada tc, disciplina_turma_compartilhada dtc, semestre s
               		      where s.ativo = true and 
               		    	    tc.semestre = s.codigo and
               		    	    dtc.turma_compartilhada = tc.codigo and
               		    	    tc.codigo = (select dtc.turma_compartilhada from disciplina_turma_compartilhada dtc where dtc.disciplina = p_disciplina)
               		   )

   loop
      -- adicionando as aulas (exceto da turma que provocou o registro da aula)
      if (r.disciplina <> p_disciplina) then
	 --raise notice 'insert into aula(horario, dia, turma) values (%, %, %);', p_horario, p_dia, r.codigo;
	 insert into aula (horario, dia, turma) values (p_horario, p_dia, r.codigo);
      end if;
   end loop;

   return true;
end;
$$;


ALTER FUNCTION public.addaulasturmascompartilhadas(p_disciplina integer, p_dia integer, p_horario integer) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 26903)
-- Name: adddisponibilidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION adddisponibilidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
   if new.disp_segunda is null then
      new.disp_segunda = true;
   end if;
   
   if new.disp_terca is null then
      new.disp_terca = true;
   end if;
   
   if new.disp_quarta is null then
      new.disp_quarta = true;
   end if;
   
   if new.disp_quinta is null then
      new.disp_quinta = true;
   end if;
   
   if new.disp_sexta is null then
      new.disp_sexta = true;
   end if;
   
   return new;
end;
$$;


ALTER FUNCTION public.adddisponibilidade() OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 26904)
-- Name: addsemestre(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION addsemestre() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare r record;
begin
   select into r * from semestre where ano = new.ano and periodo = new.periodo;
   if r is not null then
      raise exception 'Semestre %.%, já está registrado no sistema.', new.ano, new.periodo;
   end if;
   
   new.ativo = false;   
   return new;
end;
$$;


ALTER FUNCTION public.addsemestre() OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 26905)
-- Name: addsemestredisponibilidadeprofessor(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION addsemestredisponibilidadeprofessor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare p record;
begin
   for p in select codigo from professor where ativo = true
   loop
	insert into disponibilidade(professor, semestre, restricao) values (p.codigo, new.codigo, 'Definida no Semestre');
   end loop;
   
   new.ativo = false;   
   return new;
end;
$$;


ALTER FUNCTION public.addsemestredisponibilidadeprofessor() OWNER TO postgres;

--
-- TOC entry 254 (class 1255 OID 26906)
-- Name: addusuario(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION addusuario() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare r record;
begin
   select into r * from usuario where login = new.login;
   if r is not null then
      raise exception 'Login %, já está registrado no sistema.', new.login;
   end if;
   new.admin = false;
   return new;
end;
$$;


ALTER FUNCTION public.addusuario() OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 26907)
-- Name: atualizasemestre(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION atualizasemestre() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare r record;
begin
   select into r * from semestre where ativo = true;
   if (r is not null) then
      if (r.codigo <> new.codigo) then
         raise exception 'Primeiro é necessário desativar o semestre %.%.', r.ano, r.periodo;
      end if ;
   end if;

   return NEW;
end;
$$;


ALTER FUNCTION public.atualizasemestre() OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 26908)
-- Name: carga_horaria_professor(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION carga_horaria_professor(cod_prof integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
   declare ch_normal integer;
   declare ch_compartilhada integer;
   declare soma integer;
begin
   -- obtendo a carga horária normal
   select into ch_normal sum(d.aulas_semana) from disciplina d, professor p, semestre s, turma t 
											 where p.codigo = cod_prof and t.professor = p.codigo and
												   t.disciplina = d.codigo and t.semestre = s.codigo and
												   s.ativo = true and t.identificacao not like 'X';
	
   -- obtendo a carga horária compartilhada
   select into ch_compartilhada sum(tc.aulas_semana) from turma_compartilhada tc, semestre s where s.ativo = true and tc.semestre = s.codigo and tc.professor = cod_prof;

   if (ch_normal is not NULL) then
      soma = ch_normal;
   else
      soma = 0;
   end if;

   if (ch_compartilhada is not NULL) then
      soma = soma + ch_compartilhada;
   end if;
   
   return soma;  
end;
$$;


ALTER FUNCTION public.carga_horaria_professor(cod_prof integer) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 26909)
-- Name: cria_turmas_compartilhadas(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cria_turmas_compartilhadas() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  declare tc record;
begin
   -- obter informação (semestre e professor) da turma compartilhada
   select into tc professor, semestre from turma_compartilhada where codigo = new.turma_compartilhada;

   -- criando uma turma compartilhada
   insert into turma (identificacao, disciplina, semestre, professor, disciplina_turma_compartilhada) values ('X', new.disciplina, tc.semestre, tc.professor, new.codigo);

   -- inserir as aulas

   return new;
end;
$$;


ALTER FUNCTION public.cria_turmas_compartilhadas() OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 26910)
-- Name: del_disciplina_turma_vinculada(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION del_disciplina_turma_vinculada() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   declare cod integer;
begin
   -- obter a turma 
   select into cod codigo from turma where disciplina_turma_compartilhada = old.codigo;
   
   -- remove as aulas daquela turma
   delete from aula where turma = cod;

   -- remove a turma criada
   delete from turma where disciplina_turma_compartilhada = old.codigo;
   
   return old;
end;
$$;


ALTER FUNCTION public.del_disciplina_turma_vinculada() OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 26911)
-- Name: deletaaula(integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deletaaula(p_curso integer, p_periodo integer, p_dia integer, p_horario integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare r record;
declare rec record;
begin
   -- obtendo o código da disciplina que está sendo removida a aula
   select into r codcurso, coddisc, codturma, turma, codhorario, coddia from aulas where codcurso = p_curso and periodo = p_periodo and coddia = p_dia and codhorario = p_horario;

   -- verificando se tem aula registrada para o curso/período/dia/horário especificado.
   if (r is not null) then
        -- verificando se é turma compartilhada
	if (r.turma = 'X') then
	   for rec in select t.codigo, t.identificacao, t.disciplina, t.semestre, t.professor, c.codigo as codcurso, d.periodo as periodo, c.abreviacao as abrevcurso
		    from turma t, disciplina d, curso c, semestre s
		    where s.ativo = true and t.semestre = s.codigo and
		          t.disciplina = d.codigo and
			  d.curso = c.codigo and
			  t.identificacao = 'X' and
			  d.codigo in (
               		      select dtc.disciplina
               		      from turma_compartilhada tc, disciplina_turma_compartilhada dtc, semestre s
               		      where s.ativo = true and 
               		    	    tc.semestre = s.codigo and
               		    	    dtc.turma_compartilhada = tc.codigo and
               		    	    tc.codigo = (select dtc.turma_compartilhada from disciplina_turma_compartilhada dtc where dtc.disciplina = r.coddisc)
               		   )
             loop
                -- todas as aulas devem ser removidas da turma compartilhada
		--raise notice 'Turma (%)% - Curso % - Periodo %', rec.identificacao, rec.codigo, rec.codcurso, rec.periodo;
		delete from aula where turma = rec.codigo and horario = p_horario and dia = p_dia;
             end loop;
        else
           delete from aula
	   where horario = p_horario and dia = p_dia and
		 turma in ( select t.codigo 
			    from aula a, turma t, disciplina d, curso c, semestre s
			    where a.horario = p_horario and 
			          a.dia = p_dia and
			          a.turma = t.codigo and
			          s.ativo = true and
			          t.semestre = s.codigo and
			          t.disciplina = d.codigo and
			          d.curso = c.codigo and
			          c.codigo = p_curso and
			          d.periodo = p_periodo);
	end if;
   else
      raise exception 'Não existe aula registrada para esse curso/período/dia/horário.';
   end if;
      
   return true;
end;
$$;


ALTER FUNCTION public.deletaaula(p_curso integer, p_periodo integer, p_dia integer, p_horario integer) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 26912)
-- Name: moveaula(integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION moveaula(p_curso integer, p_periodo integer, p_dia_origem integer, p_horario_origem integer, p_dia_destino integer, p_horario_destino integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
   declare reg1 record;
   declare al record;
   declare reg2 record;
   declare prof1 record;
   declare prof2 record;
begin
   -- obter a(s) aula(s) que será (ão) movida(s)
   select into reg1 codaula, coddisc, periodo, codturma, turma, codprofessor from aulas where codcurso = p_curso and periodo = p_periodo and p_dia_origem = coddia and p_horario_origem = codhorario;

   --raise notice 'Codigo da aula = %', reg1.codaula;

   -- verificar se aula é de turma compartilhada
   if (reg1.turma = 'X') then
            -- obter todas as aulas que devem ser movidas
            for al in select a.codigo as aula, c.codigo as curso, d.periodo as periodo, t.professor as professor
		    from aula a, turma t, disciplina d, curso c, semestre s
		    where s.ativo = true and t.semestre = s.codigo and
		          a.turma = t.codigo and
                          a.dia = p_dia_origem and
                          a.horario = p_horario_origem and
		          t.disciplina = d.codigo and
			  d.curso = c.codigo and
			  t.identificacao = 'X' and
			  d.codigo in (
               		      select dtc.disciplina
               		      from turma_compartilhada tc, disciplina_turma_compartilhada dtc, semestre s
               		      where s.ativo = true and 
               		    	    tc.semestre = s.codigo and
               		    	    dtc.turma_compartilhada = tc.codigo and
               		    	    tc.codigo = (select dtc.turma_compartilhada from disciplina_turma_compartilhada dtc where dtc.disciplina = reg1.coddisc)
               		   )
             loop
                -- raise notice 'Verificar aula % (%)', al.aula, reg1.coddisc;
                -- verificar se pode inserir no destino
                select into reg2 curso, periodo from aulas where codcurso = al.curso and periodo = al.periodo and coddia = p_dia_destino and codhorario = p_horario_destino;
                if (reg2 is not null) then
                    raise exception 'Não é possível mover a aula compartilhada. No curso "%" (% - período) já tem aula registrada.', reg2.curso, reg2.periodo;
                end if;

                -- verifica se existe aula do professor naquele dia/horário
		select into prof1 curso, periodo, professor from aulas where codprofessor = al.professor and codhorario = p_horario_destino and coddia = p_dia_destino;
                if (prof1 is not null) then
		    raise exception 'Não é possível mover a aula compartilhada. No curso "%" (% - período) já tem aula registrada para o(a) professor(a) %', prof1.curso, prof1.periodo, prof1.professor;
                end if;
             end loop;

             -- mover as aulas
             for al in select a.codigo as aula, a.turma
		    from aula a, turma t, disciplina d, curso c, semestre s
		    where s.ativo = true and t.semestre = s.codigo and
		          a.turma = t.codigo and
                          a.dia = p_dia_origem and
                          a.horario = p_horario_origem and
		          t.disciplina = d.codigo and
			  d.curso = c.codigo and
			  t.identificacao = 'X' and
			  d.codigo in (
               		      select dtc.disciplina
               		      from turma_compartilhada tc, disciplina_turma_compartilhada dtc, semestre s
               		      where s.ativo = true and 
               		    	    tc.semestre = s.codigo and
               		    	    dtc.turma_compartilhada = tc.codigo and
               		    	    tc.codigo = (select dtc.turma_compartilhada from disciplina_turma_compartilhada dtc where dtc.disciplina = reg1.coddisc)
               		   )
             loop
                -- atualiza a aula
                update aula set dia = p_dia_destino, horario = p_horario_destino, sala = null, laboratorio = null where codigo = al.aula;
             end loop;
   else
      -- não é turma compartilhada
      -- verificar se existe aula no destino
      select into reg2 curso, periodo from aulas where codcurso = p_curso and periodo = p_periodo and coddia = p_dia_destino and codhorario = p_horario_destino;
      if (reg2 is not null) then
          raise exception 'Não é possível mover a aula. No curso "%" (% - período) já tem aula registrada.', reg2.curso, reg2.periodo;
      else
          -- pegar todas a aulas daquele horário no curso/periodo/horario/dia
	  for reg1 in select codaula, coddisc, periodo, codturma, turma, codprofessor from aulas where codcurso = p_curso and periodo = p_periodo and p_dia_origem = coddia and p_horario_origem = codhorario
	  loop
		-- verifica se existe aula do professor naquele dia/horário
		select into prof2 curso, periodo, professor from aulas where codprofessor = reg1.codprofessor and codhorario = p_horario_destino and coddia = p_dia_destino;
		if (prof2 is not null) then
			raise exception 'Não é possível mover a aula. No curso "%" (% - período) já tem aula registrada para o(a) professor(a) %', prof2.curso, prof2.periodo, prof2.professor;
		end if;
	  end loop;
          
	  -- pode! atualiza tudo !
	  for reg1 in select codaula, coddisc, periodo, codturma, turma, codprofessor from aulas where codcurso = p_curso and periodo = p_periodo and p_dia_origem = coddia and p_horario_origem = codhorario
	  loop
	     update aula set dia = p_dia_destino, horario = p_horario_destino, sala = null, laboratorio = null where codigo = reg1.codaula;
	  end loop;          
      end if;
   end if;
      
   return true;
end;
$$;


ALTER FUNCTION public.moveaula(p_curso integer, p_periodo integer, p_dia_origem integer, p_horario_origem integer, p_dia_destino integer, p_horario_destino integer) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 26913)
-- Name: propaga_ambiente(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION propaga_ambiente(cod_aula integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
   declare a record;
   declare r_aula record;
   declare t record;
begin
   -- obtendo os dados da turma
   select into t tu.codigo, tu.identificacao from turma tu, aula au where au.turma = tu.codigo and au.codigo = cod_aula;

   if (t.identificacao = 'X') then
	   -- obtendo os dados da aula
	   select into r_aula codigo, horario, dia, sala, laboratorio, turma from aula where codigo = cod_aula;

	   -- obter as aulas das turmas compartilhadas, nesse horário
	   for a in select codigo, horario, dia, sala, laboratorio, turma
	   from aula
	   where turma in (select codigo
			from turma
			where disciplina_turma_compartilhada in(select codigo
								from disciplina_turma_compartilhada 
								where turma_compartilhada = (	select turma_compartilhada 
												from disciplina_turma_compartilhada 
												where codigo =(	select disciplina_turma_compartilhada 
														from turma 
														where codigo = r_aula.turma
														)
												)
								)
			) and horario = r_aula.horario and dia = r_aula.dia and codigo != r_aula.codigo
	   loop
		update aula set sala = r_aula.sala, laboratorio = r_aula.laboratorio where codigo = a.codigo;
	   end loop;
   end if;
	
   return true;
end;
$$;


ALTER FUNCTION public.propaga_ambiente(cod_aula integer) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 26914)
-- Name: troca_professor_turma(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION troca_professor_turma(prof integer, disc integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare 
   ap record; -- aula do professor
   ad record; -- aula da disciplina

begin
   -- pega as aulas do professor
   for ap in select coddia, codhorario, professor, dia, horario from aulas where codprofessor = prof loop
      for ad in select coddia, codhorario from aulas where coddisc = disc loop
         if ((ap.coddia = ad.coddia) and (ap.codhorario = ad.codhorario)) then
            raise notice 'Dia: % - Horário: %', ap.dia, ap.horario;
            return false; -- deu erro
         end if;
      end loop;
   end loop;

   -- atualiza o professor da disciplina
   update turma set professor = prof where disciplina = disc;
    
   return true; -- sucesso
end
$$;


ALTER FUNCTION public.troca_professor_turma(prof integer, disc integer) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 26915)
-- Name: troca_professor_turma(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION troca_professor_turma(tur integer, prof integer, disc integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare 
   ap record; -- aula do professor
   ad record; -- aula da disciplina

begin
   -- pega as aulas do professor
   for ap in select coddia, codhorario, professor, dia, horario from aulas where codprofessor = prof loop
      for ad in select coddia, codhorario from aulas where coddisc = disc loop
         if ((ap.coddia = ad.coddia) and (ap.codhorario = ad.codhorario)) then
            raise notice 'Dia: % - Horário: %', ap.dia, ap.horario;
            return false; -- deu erro
         end if;
      end loop;
   end loop;

   -- atualiza o professor da disciplina
   update turma set professor = prof where codigo = tur;
    
   return true; -- sucesso
end
$$;


ALTER FUNCTION public.troca_professor_turma(tur integer, prof integer, disc integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 26916)
-- Name: apoio_ensino; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE apoio_ensino (
    codigo integer NOT NULL,
    tipo character varying(20) DEFAULT ''::character varying NOT NULL,
    aluno character varying(40) DEFAULT ''::character varying NOT NULL,
    carga_horaria integer NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    curso character varying(20) DEFAULT ''::character varying,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.apoio_ensino OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 26923)
-- Name: apoio_ensino_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE apoio_ensino_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.apoio_ensino_codigo_seq OWNER TO postgres;

--
-- TOC entry 2566 (class 0 OID 0)
-- Dependencies: 171
-- Name: apoio_ensino_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE apoio_ensino_codigo_seq OWNED BY apoio_ensino.codigo;


--
-- TOC entry 172 (class 1259 OID 26925)
-- Name: atividade_complementar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE atividade_complementar (
    codigo integer NOT NULL,
    descricao character varying(80) NOT NULL,
    carga_horaria integer NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.atividade_complementar OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 26929)
-- Name: atividade_complementar_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE atividade_complementar_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atividade_complementar_codigo_seq OWNER TO postgres;

--
-- TOC entry 2567 (class 0 OID 0)
-- Dependencies: 173
-- Name: atividade_complementar_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE atividade_complementar_codigo_seq OWNED BY atividade_complementar.codigo;


--
-- TOC entry 174 (class 1259 OID 26931)
-- Name: aula; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE aula (
    codigo integer NOT NULL,
    horario integer NOT NULL,
    dia integer NOT NULL,
    sala integer,
    laboratorio integer,
    turma integer NOT NULL,
    semestre integer,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.aula OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 26935)
-- Name: aula_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE aula_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aula_codigo_seq OWNER TO postgres;

--
-- TOC entry 2568 (class 0 OID 0)
-- Dependencies: 175
-- Name: aula_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE aula_codigo_seq OWNED BY aula.codigo;


--
-- TOC entry 176 (class 1259 OID 26937)
-- Name: curso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE curso (
    codigo integer NOT NULL,
    unidade integer NOT NULL,
    descricao character varying(40) NOT NULL,
    abreviacao character varying(10) NOT NULL,
    periodos integer NOT NULL,
    public boolean DEFAULT false NOT NULL,
    academico integer,
    turno integer,
    nivel integer
);


ALTER TABLE public.curso OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 26941)
-- Name: dia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dia (
    codigo integer NOT NULL,
    descricao character varying(40) NOT NULL,
    abreviacao character varying(10) NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.dia OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 26945)
-- Name: disciplina; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disciplina (
    codigo integer NOT NULL,
    curso integer NOT NULL,
    descricao character varying(42) NOT NULL,
    abreviacao character varying(14) NOT NULL,
    periodo integer NOT NULL,
    carga_horaria integer NOT NULL,
    aulas_semana integer NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.disciplina OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 26949)
-- Name: horario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE horario (
    codigo integer NOT NULL,
    descricao character varying(2) NOT NULL,
    inicio time without time zone NOT NULL,
    fim time without time zone NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.horario OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 26953)
-- Name: laboratorio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE laboratorio (
    codigo integer NOT NULL,
    descricao character varying(40) NOT NULL,
    abreviacao character varying(10) NOT NULL,
    public boolean DEFAULT false NOT NULL,
    capacidade integer
);


ALTER TABLE public.laboratorio OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 26957)
-- Name: professor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE professor (
    codigo integer NOT NULL,
    nome character varying(40) NOT NULL,
    abreviacao character varying(14) NOT NULL,
    unidade integer NOT NULL,
    vinculo integer NOT NULL,
    regime integer NOT NULL,
    ativo boolean NOT NULL,
    email character varying(200),
    telefone character varying(50),
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.professor OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 26961)
-- Name: sala; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sala (
    codigo integer NOT NULL,
    descricao character varying(40) NOT NULL,
    abreviacao character varying(10) NOT NULL,
    public boolean DEFAULT false NOT NULL,
    capacidade integer
);


ALTER TABLE public.sala OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 26965)
-- Name: semestre; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE semestre (
    codigo integer NOT NULL,
    ano integer NOT NULL,
    periodo integer NOT NULL,
    ativo boolean NOT NULL
);


ALTER TABLE public.semestre OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 26968)
-- Name: turma; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE turma (
    codigo integer NOT NULL,
    identificacao character varying(1) NOT NULL,
    disciplina integer NOT NULL,
    semestre integer NOT NULL,
    professor integer NOT NULL,
    public boolean DEFAULT false NOT NULL,
    disciplina_turma_compartilhada integer
);


ALTER TABLE public.turma OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 26972)
-- Name: aulas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW aulas AS
 SELECT a.codigo AS codaula,
    c.codigo AS codcurso,
    c.abreviacao AS curso,
    d.periodo,
    d.codigo AS coddisc,
    d.abreviacao AS disciplina,
    t.codigo AS codturma,
    t.identificacao AS turma,
    p.codigo AS codprofessor,
    p.abreviacao AS professor,
    h.descricao AS horario,
    h.codigo AS codhorario,
    di.abreviacao AS dia,
    di.codigo AS coddia,
    ((s.ano || '.'::text) || s.periodo) AS semestreativo,
    ( SELECT sala.abreviacao
           FROM sala
          WHERE (sala.codigo = a.sala)) AS sala,
    ( SELECT laboratorio.abreviacao
           FROM laboratorio
          WHERE (laboratorio.codigo = a.laboratorio)) AS laboratorio
   FROM curso c,
    disciplina d,
    turma t,
    aula a,
    professor p,
    horario h,
    dia di,
    semestre s
  WHERE ((((((((a.turma = t.codigo) AND (t.disciplina = d.codigo)) AND (d.curso = c.codigo)) AND (t.professor = p.codigo)) AND (a.horario = h.codigo)) AND (a.dia = di.codigo)) AND (s.ativo = true)) AND (t.semestre = s.codigo));


ALTER TABLE public.aulas OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 26977)
-- Name: aulas_com_laboratorio; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW aulas_com_laboratorio AS
 SELECT a.codigo AS codaula,
    c.codigo AS codcurso,
    c.abreviacao AS curso,
    d.codigo AS coddisciplina,
    d.abreviacao AS disciplina,
    d.carga_horaria,
    d.aulas_semana,
    h.codigo AS codhorario,
    h.descricao AS horario,
    h.inicio AS horainicio,
    h.fim AS horafim,
    di.codigo AS coddia,
    di.abreviacao AS dia,
    p.codigo AS codprofessor,
    p.abreviacao AS professor,
    s.codigo AS codsemestre,
    s.ano AS anosemestre,
    s.periodo AS periodosemestre,
    l.abreviacao AS laboratorio,
    l.codigo AS codlab,
    d.periodo,
    t.identificacao AS turma
   FROM curso c,
    disciplina d,
    aula a,
    turma t,
    horario h,
    dia di,
    professor p,
    semestre s,
    laboratorio l
  WHERE ((((((((((a.turma = t.codigo) AND (t.disciplina = d.codigo)) AND (d.curso = c.codigo)) AND (a.horario = h.codigo)) AND (a.dia = di.codigo)) AND (t.professor = p.codigo)) AND (s.ativo = true)) AND (t.semestre = s.codigo)) AND (a.laboratorio IS NOT NULL)) AND (a.laboratorio = l.codigo));


ALTER TABLE public.aulas_com_laboratorio OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 26982)
-- Name: aulas_com_sala; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW aulas_com_sala AS
 SELECT a.codigo AS codaula,
    c.codigo AS codcurso,
    c.abreviacao AS curso,
    d.codigo AS coddisciplina,
    d.abreviacao AS disciplina,
    d.carga_horaria,
    d.aulas_semana,
    h.codigo AS codhorario,
    h.descricao AS horario,
    h.inicio AS horainicio,
    h.fim AS horafim,
    di.codigo AS coddia,
    di.abreviacao AS dia,
    p.codigo AS codprofessor,
    p.abreviacao AS professor,
    s.codigo AS codsemestre,
    s.ano AS anosemestre,
    s.periodo AS periodosemestre,
    sa.abreviacao AS sala,
    sa.codigo AS codsala,
    d.periodo,
    t.identificacao AS turma
   FROM curso c,
    disciplina d,
    aula a,
    turma t,
    horario h,
    dia di,
    professor p,
    semestre s,
    sala sa
  WHERE ((((((((((a.turma = t.codigo) AND (t.disciplina = d.codigo)) AND (d.curso = c.codigo)) AND (a.horario = h.codigo)) AND (a.dia = di.codigo)) AND (t.professor = p.codigo)) AND (s.ativo = true)) AND (t.semestre = s.codigo)) AND (a.sala IS NOT NULL)) AND (a.sala = sa.codigo));


ALTER TABLE public.aulas_com_sala OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 26987)
-- Name: aulas_sem_laboratorio; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW aulas_sem_laboratorio AS
 SELECT a.codigo AS codaula,
    c.codigo AS codcurso,
    c.abreviacao AS curso,
    d.codigo AS coddisciplina,
    d.abreviacao AS disciplina,
    d.carga_horaria,
    d.aulas_semana,
    h.codigo AS codhorario,
    h.descricao AS horario,
    h.inicio AS horainicio,
    h.fim AS horafim,
    di.codigo AS coddia,
    di.abreviacao AS dia,
    p.codigo AS codprofessor,
    p.abreviacao AS professor,
    s.codigo AS codsemestre,
    s.ano AS anosemestre,
    s.periodo AS periodosemestre,
    d.periodo,
    t.identificacao AS turma
   FROM curso c,
    disciplina d,
    aula a,
    turma t,
    horario h,
    dia di,
    professor p,
    semestre s
  WHERE (((((((((a.turma = t.codigo) AND (t.disciplina = d.codigo)) AND (d.curso = c.codigo)) AND (a.horario = h.codigo)) AND (a.dia = di.codigo)) AND (t.professor = p.codigo)) AND (s.ativo = true)) AND (t.semestre = s.codigo)) AND (a.laboratorio IS NULL));


ALTER TABLE public.aulas_sem_laboratorio OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 26992)
-- Name: aulas_sem_sala; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW aulas_sem_sala AS
 SELECT a.codigo AS codaula,
    c.codigo AS codcurso,
    c.abreviacao AS curso,
    d.codigo AS coddisciplina,
    d.abreviacao AS disciplina,
    d.carga_horaria,
    d.aulas_semana,
    h.codigo AS codhorario,
    h.descricao AS horario,
    h.inicio AS horainicio,
    h.fim AS horafim,
    di.codigo AS coddia,
    di.abreviacao AS dia,
    p.codigo AS codprofessor,
    p.abreviacao AS professor,
    s.codigo AS codsemestre,
    s.ano AS anosemestre,
    s.periodo AS periodosemestre,
    d.periodo,
    t.identificacao AS turma
   FROM curso c,
    disciplina d,
    aula a,
    turma t,
    horario h,
    dia di,
    professor p,
    semestre s
  WHERE (((((((((a.turma = t.codigo) AND (t.disciplina = d.codigo)) AND (d.curso = c.codigo)) AND (a.horario = h.codigo)) AND (a.dia = di.codigo)) AND (t.professor = p.codigo)) AND (s.ativo = true)) AND (t.semestre = s.codigo)) AND (a.sala IS NULL));


ALTER TABLE public.aulas_sem_sala OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 26997)
-- Name: campus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE campus (
    codigo integer NOT NULL,
    nome character varying(40)
);


ALTER TABLE public.campus OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 27000)
-- Name: campus_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE campus_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campus_codigo_seq OWNER TO postgres;

--
-- TOC entry 2569 (class 0 OID 0)
-- Dependencies: 191
-- Name: campus_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE campus_codigo_seq OWNED BY campus.codigo;


--
-- TOC entry 192 (class 1259 OID 27002)
-- Name: capacitacao; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE capacitacao (
    codigo integer NOT NULL,
    tipo character varying(20) NOT NULL,
    instituicao character varying(40) NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    inicio character varying(20) NOT NULL,
    fim character varying(20) NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.capacitacao OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 27006)
-- Name: capacitacao_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE capacitacao_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.capacitacao_codigo_seq OWNER TO postgres;

--
-- TOC entry 2570 (class 0 OID 0)
-- Dependencies: 193
-- Name: capacitacao_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE capacitacao_codigo_seq OWNED BY capacitacao.codigo;


--
-- TOC entry 194 (class 1259 OID 27008)
-- Name: curso_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE curso_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.curso_codigo_seq OWNER TO postgres;

--
-- TOC entry 2571 (class 0 OID 0)
-- Dependencies: 194
-- Name: curso_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE curso_codigo_seq OWNED BY curso.codigo;


--
-- TOC entry 195 (class 1259 OID 27010)
-- Name: dia_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dia_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dia_codigo_seq OWNER TO postgres;

--
-- TOC entry 2572 (class 0 OID 0)
-- Dependencies: 195
-- Name: dia_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dia_codigo_seq OWNED BY dia.codigo;


--
-- TOC entry 196 (class 1259 OID 27012)
-- Name: disciplina_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disciplina_codigo_seq OWNER TO postgres;

--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 196
-- Name: disciplina_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_codigo_seq OWNED BY disciplina.codigo;


--
-- TOC entry 197 (class 1259 OID 27014)
-- Name: disciplina_formulario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disciplina_formulario (
    codigo integer NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    descricao character varying(400) NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.disciplina_formulario OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 27018)
-- Name: disciplina_formulario_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_formulario_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disciplina_formulario_codigo_seq OWNER TO postgres;

--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 198
-- Name: disciplina_formulario_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_formulario_codigo_seq OWNED BY disciplina_formulario.codigo;


--
-- TOC entry 199 (class 1259 OID 27020)
-- Name: disciplina_turma_compartilhada; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disciplina_turma_compartilhada (
    codigo integer NOT NULL,
    turma_compartilhada integer NOT NULL,
    disciplina integer NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.disciplina_turma_compartilhada OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 27024)
-- Name: disciplina_turma_compartilhada_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_turma_compartilhada_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disciplina_turma_compartilhada_codigo_seq OWNER TO postgres;

--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 200
-- Name: disciplina_turma_compartilhada_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_turma_compartilhada_codigo_seq OWNED BY disciplina_turma_compartilhada.codigo;


--
-- TOC entry 201 (class 1259 OID 27026)
-- Name: disponibilidade; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disponibilidade (
    codigo integer NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    disp_segunda boolean NOT NULL,
    disp_terca boolean NOT NULL,
    disp_quarta boolean NOT NULL,
    disp_quinta boolean NOT NULL,
    disp_sexta boolean NOT NULL,
    restricao character varying(1000),
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.disponibilidade OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 27033)
-- Name: disponibilidade_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disponibilidade_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disponibilidade_codigo_seq OWNER TO postgres;

--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 202
-- Name: disponibilidade_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disponibilidade_codigo_seq OWNED BY disponibilidade.codigo;


--
-- TOC entry 203 (class 1259 OID 27035)
-- Name: disponibilidade_novo_2014; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disponibilidade_novo_2014 (
    codigo integer NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    disp_segunda boolean NOT NULL,
    disp_terca boolean NOT NULL,
    disp_quarta boolean NOT NULL,
    disp_quinta boolean NOT NULL,
    disp_sexta boolean NOT NULL,
    restricao text,
    dataresposta timestamp without time zone NOT NULL
);


ALTER TABLE public.disponibilidade_novo_2014 OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 27041)
-- Name: extensao; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE extensao (
    codigo integer NOT NULL,
    titulo character varying(120) NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.extensao OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 27045)
-- Name: extensao_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE extensao_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.extensao_codigo_seq OWNER TO postgres;

--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 205
-- Name: extensao_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE extensao_codigo_seq OWNED BY extensao.codigo;


--
-- TOC entry 206 (class 1259 OID 27047)
-- Name: horario_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE horario_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.horario_codigo_seq OWNER TO postgres;

--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 206
-- Name: horario_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE horario_codigo_seq OWNED BY horario.codigo;


--
-- TOC entry 207 (class 1259 OID 27049)
-- Name: horario_individual; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW horario_individual AS
 SELECT a.codigo AS codaula,
    c.codigo AS codcurso,
    c.abreviacao AS curso,
    d.codigo AS coddisciplina,
    d.abreviacao AS disciplina,
    d.periodo,
    d.carga_horaria,
    d.aulas_semana,
    h.codigo AS codhorario,
    h.descricao AS horario,
    h.inicio AS horainicio,
    h.fim AS horafim,
    di.codigo AS coddia,
    di.abreviacao AS dia,
    p.codigo AS codprofessor,
    p.abreviacao AS professor,
    s.codigo AS codsemestre,
    s.ano AS anosemestre,
    s.periodo AS periodosemestre,
    ( SELECT sala.abreviacao
           FROM sala
          WHERE (sala.codigo = a.sala)) AS sala,
    ( SELECT laboratorio.abreviacao
           FROM laboratorio
          WHERE (laboratorio.codigo = a.laboratorio)) AS laboratorio
   FROM curso c,
    disciplina d,
    aula a,
    turma t,
    horario h,
    dia di,
    professor p,
    semestre s
  WHERE ((((((((a.turma = t.codigo) AND (t.disciplina = d.codigo)) AND (d.curso = c.codigo)) AND (a.horario = h.codigo)) AND (a.dia = di.codigo)) AND (t.professor = p.codigo)) AND (s.ativo = true)) AND (t.semestre = s.codigo));


ALTER TABLE public.horario_individual OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 27054)
-- Name: laboratorio_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE laboratorio_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.laboratorio_codigo_seq OWNER TO postgres;

--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 208
-- Name: laboratorio_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE laboratorio_codigo_seq OWNED BY laboratorio.codigo;


--
-- TOC entry 209 (class 1259 OID 27056)
-- Name: log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log (
    codigo integer NOT NULL,
    nome character varying(40),
    atividade text,
    datahora timestamp without time zone
);


ALTER TABLE public.log OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 27062)
-- Name: log_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_codigo_seq OWNER TO postgres;

--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 210
-- Name: log_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_codigo_seq OWNED BY log.codigo;


--
-- TOC entry 211 (class 1259 OID 27064)
-- Name: nivel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nivel (
    codigo integer NOT NULL,
    descricao character varying(20)
);


ALTER TABLE public.nivel OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 27067)
-- Name: nivel_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE nivel_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_codigo_seq OWNER TO postgres;

--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 212
-- Name: nivel_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE nivel_codigo_seq OWNED BY nivel.codigo;


--
-- TOC entry 213 (class 1259 OID 27069)
-- Name: pesquisa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pesquisa (
    codigo integer NOT NULL,
    titulo character varying(120) NOT NULL,
    professor integer NOT NULL,
    semestre integer NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.pesquisa OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 27073)
-- Name: pesquisa_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pesquisa_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pesquisa_codigo_seq OWNER TO postgres;

--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 214
-- Name: pesquisa_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pesquisa_codigo_seq OWNED BY pesquisa.codigo;


--
-- TOC entry 215 (class 1259 OID 27075)
-- Name: professor_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE professor_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professor_codigo_seq OWNER TO postgres;

--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 215
-- Name: professor_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE professor_codigo_seq OWNED BY professor.codigo;


--
-- TOC entry 216 (class 1259 OID 27077)
-- Name: regime; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE regime (
    codigo integer NOT NULL,
    descricao character varying(6) NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.regime OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 27081)
-- Name: regime_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE regime_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regime_codigo_seq OWNER TO postgres;

--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 217
-- Name: regime_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE regime_codigo_seq OWNED BY regime.codigo;


--
-- TOC entry 218 (class 1259 OID 27083)
-- Name: sala_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sala_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sala_codigo_seq OWNER TO postgres;

--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 218
-- Name: sala_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sala_codigo_seq OWNED BY sala.codigo;


--
-- TOC entry 219 (class 1259 OID 27085)
-- Name: semestre_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE semestre_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.semestre_codigo_seq OWNER TO postgres;

--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 219
-- Name: semestre_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE semestre_codigo_seq OWNED BY semestre.codigo;


--
-- TOC entry 220 (class 1259 OID 27087)
-- Name: turma_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE turma_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turma_codigo_seq OWNER TO postgres;

--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 220
-- Name: turma_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE turma_codigo_seq OWNED BY turma.codigo;


--
-- TOC entry 221 (class 1259 OID 27089)
-- Name: turma_compartilhada; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE turma_compartilhada (
    codigo integer NOT NULL,
    descricao character varying(14) NOT NULL,
    carga_horaria integer NOT NULL,
    aulas_semana integer NOT NULL,
    professor integer,
    semestre integer,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.turma_compartilhada OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 27093)
-- Name: turma_compartilhada_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE turma_compartilhada_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turma_compartilhada_codigo_seq OWNER TO postgres;

--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 222
-- Name: turma_compartilhada_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE turma_compartilhada_codigo_seq OWNED BY turma_compartilhada.codigo;


--
-- TOC entry 223 (class 1259 OID 27095)
-- Name: turno; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE turno (
    codigo integer NOT NULL,
    descricao character varying(20),
    abreviacao character varying(4)
);


ALTER TABLE public.turno OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 27098)
-- Name: turno_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE turno_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turno_codigo_seq OWNER TO postgres;

--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 224
-- Name: turno_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE turno_codigo_seq OWNED BY turno.codigo;


--
-- TOC entry 225 (class 1259 OID 27100)
-- Name: unidade; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE unidade (
    codigo integer NOT NULL,
    descricao character varying(40) NOT NULL,
    abreviacao character varying(10) NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.unidade OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 27104)
-- Name: unidade_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE unidade_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidade_codigo_seq OWNER TO postgres;

--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 226
-- Name: unidade_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE unidade_codigo_seq OWNED BY unidade.codigo;


--
-- TOC entry 227 (class 1259 OID 27106)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    codigo integer NOT NULL,
    login character varying(14) NOT NULL,
    senha character varying(40) NOT NULL,
    admin boolean NOT NULL,
    nome character varying(20) NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 27109)
-- Name: usuario_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_codigo_seq OWNER TO postgres;

--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 228
-- Name: usuario_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_codigo_seq OWNED BY usuario.codigo;


--
-- TOC entry 229 (class 1259 OID 27111)
-- Name: vinculo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vinculo (
    codigo integer NOT NULL,
    descricao character varying(12) NOT NULL,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.vinculo OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 27115)
-- Name: vinculo_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vinculo_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vinculo_codigo_seq OWNER TO postgres;

--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 230
-- Name: vinculo_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE vinculo_codigo_seq OWNED BY vinculo.codigo;


--
-- TOC entry 2300 (class 2604 OID 27117)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apoio_ensino ALTER COLUMN codigo SET DEFAULT nextval('apoio_ensino_codigo_seq'::regclass);


--
-- TOC entry 2302 (class 2604 OID 27118)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atividade_complementar ALTER COLUMN codigo SET DEFAULT nextval('atividade_complementar_codigo_seq'::regclass);


--
-- TOC entry 2304 (class 2604 OID 27119)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula ALTER COLUMN codigo SET DEFAULT nextval('aula_codigo_seq'::regclass);


--
-- TOC entry 2322 (class 2604 OID 27120)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY campus ALTER COLUMN codigo SET DEFAULT nextval('campus_codigo_seq'::regclass);


--
-- TOC entry 2324 (class 2604 OID 27121)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY capacitacao ALTER COLUMN codigo SET DEFAULT nextval('capacitacao_codigo_seq'::regclass);


--
-- TOC entry 2306 (class 2604 OID 27122)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY curso ALTER COLUMN codigo SET DEFAULT nextval('curso_codigo_seq'::regclass);


--
-- TOC entry 2308 (class 2604 OID 27123)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dia ALTER COLUMN codigo SET DEFAULT nextval('dia_codigo_seq'::regclass);


--
-- TOC entry 2310 (class 2604 OID 27124)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina ALTER COLUMN codigo SET DEFAULT nextval('disciplina_codigo_seq'::regclass);


--
-- TOC entry 2326 (class 2604 OID 27125)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_formulario ALTER COLUMN codigo SET DEFAULT nextval('disciplina_formulario_codigo_seq'::regclass);


--
-- TOC entry 2328 (class 2604 OID 27126)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_turma_compartilhada ALTER COLUMN codigo SET DEFAULT nextval('disciplina_turma_compartilhada_codigo_seq'::regclass);


--
-- TOC entry 2330 (class 2604 OID 27127)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disponibilidade ALTER COLUMN codigo SET DEFAULT nextval('disponibilidade_codigo_seq'::regclass);


--
-- TOC entry 2332 (class 2604 OID 27128)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY extensao ALTER COLUMN codigo SET DEFAULT nextval('extensao_codigo_seq'::regclass);


--
-- TOC entry 2312 (class 2604 OID 27129)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY horario ALTER COLUMN codigo SET DEFAULT nextval('horario_codigo_seq'::regclass);


--
-- TOC entry 2314 (class 2604 OID 27130)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY laboratorio ALTER COLUMN codigo SET DEFAULT nextval('laboratorio_codigo_seq'::regclass);


--
-- TOC entry 2333 (class 2604 OID 27131)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log ALTER COLUMN codigo SET DEFAULT nextval('log_codigo_seq'::regclass);


--
-- TOC entry 2334 (class 2604 OID 27132)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nivel ALTER COLUMN codigo SET DEFAULT nextval('nivel_codigo_seq'::regclass);


--
-- TOC entry 2336 (class 2604 OID 27133)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pesquisa ALTER COLUMN codigo SET DEFAULT nextval('pesquisa_codigo_seq'::regclass);


--
-- TOC entry 2316 (class 2604 OID 27134)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY professor ALTER COLUMN codigo SET DEFAULT nextval('professor_codigo_seq'::regclass);


--
-- TOC entry 2338 (class 2604 OID 27135)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY regime ALTER COLUMN codigo SET DEFAULT nextval('regime_codigo_seq'::regclass);


--
-- TOC entry 2318 (class 2604 OID 27136)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sala ALTER COLUMN codigo SET DEFAULT nextval('sala_codigo_seq'::regclass);


--
-- TOC entry 2319 (class 2604 OID 27137)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY semestre ALTER COLUMN codigo SET DEFAULT nextval('semestre_codigo_seq'::regclass);


--
-- TOC entry 2321 (class 2604 OID 27138)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma ALTER COLUMN codigo SET DEFAULT nextval('turma_codigo_seq'::regclass);


--
-- TOC entry 2340 (class 2604 OID 27139)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma_compartilhada ALTER COLUMN codigo SET DEFAULT nextval('turma_compartilhada_codigo_seq'::regclass);


--
-- TOC entry 2341 (class 2604 OID 27140)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turno ALTER COLUMN codigo SET DEFAULT nextval('turno_codigo_seq'::regclass);


--
-- TOC entry 2343 (class 2604 OID 27141)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidade ALTER COLUMN codigo SET DEFAULT nextval('unidade_codigo_seq'::regclass);


--
-- TOC entry 2344 (class 2604 OID 27142)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN codigo SET DEFAULT nextval('usuario_codigo_seq'::regclass);


--
-- TOC entry 2346 (class 2604 OID 27143)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vinculo ALTER COLUMN codigo SET DEFAULT nextval('vinculo_codigo_seq'::regclass);


--
-- TOC entry 2382 (class 2606 OID 27145)
-- Name: disponibilidade_novo_2014_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY disponibilidade_novo_2014
    ADD CONSTRAINT disponibilidade_novo_2014_pkey PRIMARY KEY (codigo);


--
-- TOC entry 2348 (class 2606 OID 27147)
-- Name: pk_apoio_ensino; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY apoio_ensino
    ADD CONSTRAINT pk_apoio_ensino PRIMARY KEY (codigo);


--
-- TOC entry 2350 (class 2606 OID 27149)
-- Name: pk_atividade_complementar; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY atividade_complementar
    ADD CONSTRAINT pk_atividade_complementar PRIMARY KEY (codigo);


--
-- TOC entry 2352 (class 2606 OID 27151)
-- Name: pk_aula; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT pk_aula PRIMARY KEY (codigo);


--
-- TOC entry 2372 (class 2606 OID 27153)
-- Name: pk_campus; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY campus
    ADD CONSTRAINT pk_campus PRIMARY KEY (codigo);


--
-- TOC entry 2374 (class 2606 OID 27155)
-- Name: pk_capacitacao; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY capacitacao
    ADD CONSTRAINT pk_capacitacao PRIMARY KEY (codigo);


--
-- TOC entry 2354 (class 2606 OID 27157)
-- Name: pk_curso; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY curso
    ADD CONSTRAINT pk_curso PRIMARY KEY (codigo);


--
-- TOC entry 2356 (class 2606 OID 27159)
-- Name: pk_dia; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dia
    ADD CONSTRAINT pk_dia PRIMARY KEY (codigo);


--
-- TOC entry 2358 (class 2606 OID 27161)
-- Name: pk_disciplina; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY disciplina
    ADD CONSTRAINT pk_disciplina PRIMARY KEY (codigo);


--
-- TOC entry 2376 (class 2606 OID 27163)
-- Name: pk_disciplina_formulario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY disciplina_formulario
    ADD CONSTRAINT pk_disciplina_formulario PRIMARY KEY (codigo);


--
-- TOC entry 2378 (class 2606 OID 27165)
-- Name: pk_disciplina_turma_compartilhada; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY disciplina_turma_compartilhada
    ADD CONSTRAINT pk_disciplina_turma_compartilhada PRIMARY KEY (codigo);


--
-- TOC entry 2380 (class 2606 OID 27167)
-- Name: pk_disponibilidade; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY disponibilidade
    ADD CONSTRAINT pk_disponibilidade PRIMARY KEY (codigo);


--
-- TOC entry 2384 (class 2606 OID 27169)
-- Name: pk_extensao; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY extensao
    ADD CONSTRAINT pk_extensao PRIMARY KEY (codigo);


--
-- TOC entry 2360 (class 2606 OID 27171)
-- Name: pk_horario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY horario
    ADD CONSTRAINT pk_horario PRIMARY KEY (codigo);


--
-- TOC entry 2362 (class 2606 OID 27173)
-- Name: pk_laboratorio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY laboratorio
    ADD CONSTRAINT pk_laboratorio PRIMARY KEY (codigo);


--
-- TOC entry 2386 (class 2606 OID 27175)
-- Name: pk_log; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log
    ADD CONSTRAINT pk_log PRIMARY KEY (codigo);


--
-- TOC entry 2388 (class 2606 OID 27177)
-- Name: pk_nivel; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nivel
    ADD CONSTRAINT pk_nivel PRIMARY KEY (codigo);


--
-- TOC entry 2390 (class 2606 OID 27179)
-- Name: pk_pesquisa; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pesquisa
    ADD CONSTRAINT pk_pesquisa PRIMARY KEY (codigo);


--
-- TOC entry 2364 (class 2606 OID 27181)
-- Name: pk_professor; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY professor
    ADD CONSTRAINT pk_professor PRIMARY KEY (codigo);


--
-- TOC entry 2392 (class 2606 OID 27183)
-- Name: pk_regime; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY regime
    ADD CONSTRAINT pk_regime PRIMARY KEY (codigo);


--
-- TOC entry 2366 (class 2606 OID 27185)
-- Name: pk_sala; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sala
    ADD CONSTRAINT pk_sala PRIMARY KEY (codigo);


--
-- TOC entry 2368 (class 2606 OID 27187)
-- Name: pk_semestre; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY semestre
    ADD CONSTRAINT pk_semestre PRIMARY KEY (codigo);


--
-- TOC entry 2370 (class 2606 OID 27189)
-- Name: pk_turma; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY turma
    ADD CONSTRAINT pk_turma PRIMARY KEY (codigo);


--
-- TOC entry 2394 (class 2606 OID 27191)
-- Name: pk_turma_compartilhada; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY turma_compartilhada
    ADD CONSTRAINT pk_turma_compartilhada PRIMARY KEY (codigo);


--
-- TOC entry 2396 (class 2606 OID 27193)
-- Name: pk_turno; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY turno
    ADD CONSTRAINT pk_turno PRIMARY KEY (codigo);


--
-- TOC entry 2398 (class 2606 OID 27195)
-- Name: pk_unidade; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY unidade
    ADD CONSTRAINT pk_unidade PRIMARY KEY (codigo);


--
-- TOC entry 2400 (class 2606 OID 27197)
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (codigo);


--
-- TOC entry 2402 (class 2606 OID 27199)
-- Name: pk_vinculo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vinculo
    ADD CONSTRAINT pk_vinculo PRIMARY KEY (codigo);


--
-- TOC entry 2436 (class 2620 OID 27200)
-- Name: add_aula; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER add_aula BEFORE INSERT ON aula FOR EACH ROW EXECUTE PROCEDURE add_aula();


--
-- TOC entry 2441 (class 2620 OID 27201)
-- Name: add_disciplina_turma_vinculada; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER add_disciplina_turma_vinculada BEFORE INSERT ON disciplina_turma_compartilhada FOR EACH ROW EXECUTE PROCEDURE add_disciplina_turma_vinculada();


--
-- TOC entry 2437 (class 2620 OID 27202)
-- Name: add_professor; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER add_professor BEFORE INSERT ON professor FOR EACH ROW EXECUTE PROCEDURE add_professor();


--
-- TOC entry 2440 (class 2620 OID 27203)
-- Name: add_turma; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER add_turma BEFORE INSERT ON turma FOR EACH ROW EXECUTE PROCEDURE add_turma();


--
-- TOC entry 2438 (class 2620 OID 27204)
-- Name: addsemestre; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER addsemestre BEFORE INSERT ON semestre FOR EACH ROW EXECUTE PROCEDURE addsemestre();


--
-- TOC entry 2444 (class 2620 OID 27205)
-- Name: addusuario; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER addusuario BEFORE INSERT ON usuario FOR EACH ROW EXECUTE PROCEDURE addusuario();


--
-- TOC entry 2439 (class 2620 OID 27206)
-- Name: atualizasemestre; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER atualizasemestre BEFORE UPDATE ON semestre FOR EACH ROW EXECUTE PROCEDURE atualizasemestre();


--
-- TOC entry 2442 (class 2620 OID 27207)
-- Name: cria_turmas_compartilhadas; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER cria_turmas_compartilhadas AFTER INSERT ON disciplina_turma_compartilhada FOR EACH ROW EXECUTE PROCEDURE cria_turmas_compartilhadas();


--
-- TOC entry 2443 (class 2620 OID 27208)
-- Name: del_disciplina_turma_vinculada; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER del_disciplina_turma_vinculada BEFORE DELETE ON disciplina_turma_compartilhada FOR EACH ROW EXECUTE PROCEDURE del_disciplina_turma_vinculada();


--
-- TOC entry 2407 (class 2606 OID 27209)
-- Name: dia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT dia FOREIGN KEY (dia) REFERENCES dia(codigo);


--
-- TOC entry 2419 (class 2606 OID 27214)
-- Name: fd_disciplina_turma_compartilhada; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma
    ADD CONSTRAINT fd_disciplina_turma_compartilhada FOREIGN KEY (disciplina_turma_compartilhada) REFERENCES disciplina_turma_compartilhada(codigo);


--
-- TOC entry 2415 (class 2606 OID 27219)
-- Name: fk_curso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina
    ADD CONSTRAINT fk_curso FOREIGN KEY (curso) REFERENCES curso(codigo);


--
-- TOC entry 2426 (class 2606 OID 27224)
-- Name: fk_disciplina; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_turma_compartilhada
    ADD CONSTRAINT fk_disciplina FOREIGN KEY (disciplina) REFERENCES disciplina(codigo);


--
-- TOC entry 2420 (class 2606 OID 27229)
-- Name: fk_disciplina; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma
    ADD CONSTRAINT fk_disciplina FOREIGN KEY (disciplina) REFERENCES disciplina(codigo);


--
-- TOC entry 2408 (class 2606 OID 27234)
-- Name: fk_laboratorio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT fk_laboratorio FOREIGN KEY (laboratorio) REFERENCES laboratorio(codigo);


--
-- TOC entry 2413 (class 2606 OID 27239)
-- Name: fk_nivel; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY curso
    ADD CONSTRAINT fk_nivel FOREIGN KEY (nivel) REFERENCES nivel(codigo);


--
-- TOC entry 2403 (class 2606 OID 27244)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apoio_ensino
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2405 (class 2606 OID 27249)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atividade_complementar
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2422 (class 2606 OID 27254)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY capacitacao
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2434 (class 2606 OID 27259)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma_compartilhada
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2424 (class 2606 OID 27264)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_formulario
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2428 (class 2606 OID 27269)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disponibilidade
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2430 (class 2606 OID 27274)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY extensao
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2432 (class 2606 OID 27279)
-- Name: fk_professor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pesquisa
    ADD CONSTRAINT fk_professor FOREIGN KEY (professor) REFERENCES professor(codigo);


--
-- TOC entry 2416 (class 2606 OID 27284)
-- Name: fk_regime; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY professor
    ADD CONSTRAINT fk_regime FOREIGN KEY (regime) REFERENCES regime(codigo);


--
-- TOC entry 2409 (class 2606 OID 27289)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2404 (class 2606 OID 27294)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apoio_ensino
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2406 (class 2606 OID 27299)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atividade_complementar
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2423 (class 2606 OID 27304)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY capacitacao
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2435 (class 2606 OID 27309)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma_compartilhada
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2425 (class 2606 OID 27314)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_formulario
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2429 (class 2606 OID 27319)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disponibilidade
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2431 (class 2606 OID 27324)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY extensao
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2433 (class 2606 OID 27329)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pesquisa
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2421 (class 2606 OID 27334)
-- Name: fk_semestre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY turma
    ADD CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre(codigo);


--
-- TOC entry 2410 (class 2606 OID 27339)
-- Name: fk_turma; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT fk_turma FOREIGN KEY (turma) REFERENCES turma(codigo);


--
-- TOC entry 2427 (class 2606 OID 27344)
-- Name: fk_turma; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_turma_compartilhada
    ADD CONSTRAINT fk_turma FOREIGN KEY (turma_compartilhada) REFERENCES turma_compartilhada(codigo);


--
-- TOC entry 2414 (class 2606 OID 27349)
-- Name: fk_turno; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY curso
    ADD CONSTRAINT fk_turno FOREIGN KEY (turno) REFERENCES turno(codigo);


--
-- TOC entry 2417 (class 2606 OID 27354)
-- Name: fk_unidade; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY professor
    ADD CONSTRAINT fk_unidade FOREIGN KEY (unidade) REFERENCES unidade(codigo);


--
-- TOC entry 2418 (class 2606 OID 27359)
-- Name: fk_vinculo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY professor
    ADD CONSTRAINT fk_vinculo FOREIGN KEY (vinculo) REFERENCES vinculo(codigo);


--
-- TOC entry 2411 (class 2606 OID 27364)
-- Name: horario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT horario FOREIGN KEY (horario) REFERENCES horario(codigo);


--
-- TOC entry 2412 (class 2606 OID 27369)
-- Name: sala; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aula
    ADD CONSTRAINT sala FOREIGN KEY (sala) REFERENCES sala(codigo);


--
-- TOC entry 2564 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-02-20 08:00:37 BRT

--
-- PostgreSQL database dump complete
--

