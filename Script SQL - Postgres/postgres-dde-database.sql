--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2015-02-20 08:01:02 BRT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 181 (class 3079 OID 12018)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 181
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 35381)
-- Name: appuser; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE appuser (
    username character varying(20) NOT NULL,
    passkey character varying(256) NOT NULL,
    profile character varying(10) NOT NULL
);


ALTER TABLE public.appuser OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 35447)
-- Name: course; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE course (
    code character varying(11) NOT NULL,
    name character varying(256) NOT NULL
);


ALTER TABLE public.course OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 35452)
-- Name: coursegrid; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE coursegrid (
    code character varying(11) NOT NULL,
    course character varying(11) NOT NULL,
    unitpart integer NOT NULL,
    unitname character varying(100) NOT NULL,
    unitcredits integer NOT NULL,
    unithours integer NOT NULL,
    status character varying(10) NOT NULL
);


ALTER TABLE public.coursegrid OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 35391)
-- Name: department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE department (
    code character varying(11) NOT NULL,
    name character varying(20) NOT NULL
);


ALTER TABLE public.department OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 35376)
-- Name: identity; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE identity (
    symbol character(2) NOT NULL,
    value integer NOT NULL
);


ALTER TABLE public.identity OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 35439)
-- Name: notificationevent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notificationevent (
    code character varying(11) NOT NULL,
    hashcode character varying(256) NOT NULL,
    author character varying(11) NOT NULL,
    created bigint NOT NULL,
    message text NOT NULL,
    error text,
    status character(10) NOT NULL
);


ALTER TABLE public.notificationevent OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 35396)
-- Name: teacher; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE teacher (
    code character varying(11) NOT NULL,
    name character varying(200) NOT NULL,
    abbreviation character varying(20) NOT NULL,
    email character varying(200) NOT NULL,
    department character varying(11) NOT NULL,
    phone character varying(50),
    status character(10) NOT NULL
);


ALTER TABLE public.teacher OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 35462)
-- Name: teacher_coursegrid; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE teacher_coursegrid (
    version character varying(11) NOT NULL,
    teacher character varying(11) NOT NULL,
    coursegrid character varying(11) NOT NULL
);

ALTER TABLE public.teacher_coursegrid OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 35424)
-- Name: teacher_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE teacher_user (
    teacher character varying(11) NOT NULL,
    appuser character varying(20) NOT NULL
);


ALTER TABLE public.teacher_user OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 35409)
-- Name: teacheravailability; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE teacheravailability (
    version character varying(11) NOT NULL,
    teacher character varying(11) NOT NULL,
    flag_monday bit(1) NOT NULL,
    flag_tuesday bit(1) NOT NULL,
    flag_wednesday bit(1) NOT NULL,
    flag_thursday bit(1) NOT NULL,
    flag_friday bit(1) NOT NULL,
    flag_phding bit(1) NOT NULL,
    flag_mscing bit(1) NOT NULL
);


ALTER TABLE public.teacheravailability OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 35386)
-- Name: version; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE version (
    code character varying(11) NOT NULL,
    year integer NOT NULL,
    part integer NOT NULL,
    flag_default bit(1) NOT NULL,
    created bigint NOT NULL
);


ALTER TABLE public.version OWNER TO postgres;

--
-- TOC entry 2146 (class 2606 OID 35451)
-- Name: pk_course; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY course
    ADD CONSTRAINT pk_course PRIMARY KEY (code);


--
-- TOC entry 2148 (class 2606 OID 35456)
-- Name: pk_coursegrid; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY coursegrid
    ADD CONSTRAINT pk_coursegrid PRIMARY KEY (code);


--
-- TOC entry 2136 (class 2606 OID 35395)
-- Name: pk_department; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY department
    ADD CONSTRAINT pk_department PRIMARY KEY (code);


--
-- TOC entry 2130 (class 2606 OID 35380)
-- Name: pk_identity; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY identity
    ADD CONSTRAINT pk_identity PRIMARY KEY (symbol, value);


--
-- TOC entry 2144 (class 2606 OID 35446)
-- Name: pk_notification; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notificationevent
    ADD CONSTRAINT pk_notification PRIMARY KEY (code);


--
-- TOC entry 2138 (class 2606 OID 35403)
-- Name: pk_teacher; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY teacher
    ADD CONSTRAINT pk_teacher PRIMARY KEY (code);


--
-- TOC entry 2150 (class 2606 OID 35466)
-- Name: pk_teacher_coursegrid; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY teacher_coursegrid
    ADD CONSTRAINT pk_teacher_coursegrid PRIMARY KEY (version, teacher, coursegrid);


--
-- TOC entry 2142 (class 2606 OID 35428)
-- Name: pk_teacher_user; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY teacher_user
    ADD CONSTRAINT pk_teacher_user PRIMARY KEY (teacher, appuser);


--
-- TOC entry 2140 (class 2606 OID 35413)
-- Name: pk_teacheravailability; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY teacheravailability
    ADD CONSTRAINT pk_teacheravailability PRIMARY KEY (version, teacher);


--
-- TOC entry 2132 (class 2606 OID 35385)
-- Name: pk_user; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY appuser
    ADD CONSTRAINT pk_user PRIMARY KEY (username);


--
-- TOC entry 2134 (class 2606 OID 35390)
-- Name: pk_version; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY version
    ADD CONSTRAINT pk_version PRIMARY KEY (code);


--
-- TOC entry 2156 (class 2606 OID 35457)
-- Name: fk_coursegrid_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coursegrid
    ADD CONSTRAINT fk_coursegrid_0 FOREIGN KEY (course) REFERENCES course(code);


--
-- TOC entry 2151 (class 2606 OID 35404)
-- Name: fk_teacher_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacher
    ADD CONSTRAINT fk_teacher_0 FOREIGN KEY (department) REFERENCES department(code);


--
-- TOC entry 2157 (class 2606 OID 35467)
-- Name: fk_teacher_coursegrid_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacher_coursegrid
    ADD CONSTRAINT fk_teacher_coursegrid_0 FOREIGN KEY (version) REFERENCES version(code);


--
-- TOC entry 2158 (class 2606 OID 35472)
-- Name: fk_teacher_coursegrid_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacher_coursegrid
    ADD CONSTRAINT fk_teacher_coursegrid_1 FOREIGN KEY (teacher) REFERENCES teacher(code);


--
-- TOC entry 2159 (class 2606 OID 35477)
-- Name: fk_teacher_coursegrid_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacher_coursegrid
    ADD CONSTRAINT fk_teacher_coursegrid_2 FOREIGN KEY (version) REFERENCES coursegrid(code);


--
-- TOC entry 2154 (class 2606 OID 35429)
-- Name: fk_teacher_user_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacher_user
    ADD CONSTRAINT fk_teacher_user_0 FOREIGN KEY (teacher) REFERENCES teacher(code);


--
-- TOC entry 2155 (class 2606 OID 35434)
-- Name: fk_teacher_user_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacher_user
    ADD CONSTRAINT fk_teacher_user_1 FOREIGN KEY (appuser) REFERENCES appuser(username);


--
-- TOC entry 2152 (class 2606 OID 35414)
-- Name: fk_teacheravailability_0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacheravailability
    ADD CONSTRAINT fk_teacheravailability_0 FOREIGN KEY (version) REFERENCES version(code);


--
-- TOC entry 2153 (class 2606 OID 35419)
-- Name: fk_teacheravailability_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teacheravailability
    ADD CONSTRAINT fk_teacheravailability_1 FOREIGN KEY (teacher) REFERENCES teacher(code);


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-02-20 08:01:02 BRT

--
-- PostgreSQL database dump complete
--

