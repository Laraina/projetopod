/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.servidor.pod.tolerancia.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public interface InterfaceDao extends Serializable{
    
    public boolean persist(Object t);
    public boolean update(Object t);
    public boolean delete(Object t);
    public Object find(int id, Class c);
    public List listar(String sql);
    public List consultaSimples(String sql, Map<?, ?> map);
    
}
