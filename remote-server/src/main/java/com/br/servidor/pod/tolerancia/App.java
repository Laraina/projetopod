package com.br.servidor.pod.tolerancia;

import com.br.servidor.pod.tolerancia.fachada.Fachada;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try {
            System.out.println("INICIANDO SERVIDOR DA EQUIPE DE FELIPE)");
            
            Registry registry = LocateRegistry.createRegistry(10909);
            registry.bind("fachadaJNDI", new Fachada());
            System.out.println("Conexao Estabelecida com Sucesso");
            
        } catch (RemoteException | AlreadyBoundException ex) {
            ex.printStackTrace();
        } 
    }
}
