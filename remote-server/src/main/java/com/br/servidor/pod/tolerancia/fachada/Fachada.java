/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.servidor.pod.tolerancia.fachada;

import com.br.interfaces.beans.Curso;
import com.br.interfaces.beans.Nivel;
import com.br.interfaces.beans.Professor;
import com.br.interfaces.beans.Regime;
import com.br.interfaces.beans.Turno;
import com.br.interfaces.beans.Unidade;
import com.br.interfaces.beans.Vinculo;
import com.br.interfaces.pod.InterfaceFachadaMySQL;
import com.br.servidor.pod.tolerancia.dao.DaoGenerico;
import com.br.servidor.pod.tolerancia.interfaces.InterfaceDao;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class Fachada extends UnicastRemoteObject implements InterfaceFachadaMySQL {

    private InterfaceDao dao;

    public Fachada() throws RemoteException {
        dao = new DaoGenerico();
    }

    @Override
    public boolean cadastrarCurso(Curso c) throws RemoteException {
        return dao.persist(c);
    }

    @Override
    public boolean cadastrarProfessor(Professor p) throws RemoteException {
        return dao.persist(p);
    }

    @Override
    public boolean atualizarCurso(Curso c) throws RemoteException {
        return dao.update(c);
    }

    @Override
    public boolean atualizarProfessor(Professor p) throws RemoteException {
        return dao.update(p);
    }

    @Override
    public List<Professor> listarProfessor() throws RemoteException {
        return dao.listar("listar.prof");
    }

    @Override
    public List<Curso> listarCursos() throws RemoteException {
        return dao.listar("listar.curso");
    }

    @Override
    public boolean existeProfessor(String code) throws RemoteException {

        Map<String, String> map = new HashMap<>();
        map.put("code", code);

        List<Professor> l = dao.consultaSimples("consulta.prof", map);

        return l.isEmpty() ? false : true;

    }

    @Override
    public boolean existeCurso(String code) throws RemoteException {

        Map<String, String> map = new HashMap<>();
        map.put("code", code);

        List<Curso> l = dao.consultaSimples("consultar.curso", map);

        return l.isEmpty() ? false : true;

    }

    @Override
    public Nivel buscarNivel(int id) throws RemoteException {
        return (Nivel) dao.find(id, Nivel.class);
    }

    @Override
    public Turno buscarTurno(int id) throws RemoteException {
        return (Turno) dao.find(id, Turno.class);
    }

    @Override
    public Unidade buscarUnidade(int id) throws RemoteException {
        return (Unidade) dao.find(id, Unidade.class);
    }

    @Override
    public Regime buscarRegime(int id) throws RemoteException {
        return (Regime) dao.find(id, Regime.class);
    }

    @Override
    public Vinculo buscarVinculo(int id) throws RemoteException {
        return (Vinculo) dao.find(id, Vinculo.class);
    }

    @Override
    public Curso buscarCurso(String code) throws RemoteException {

        Map<String, String> map = new HashMap<>();
        map.put("code", code);

        List<Curso> l = dao.consultaSimples("consultar.curso", map);

        return l.get(0);

    }

    @Override
    public Professor buscarProfessor(String code) throws RemoteException {
        
        Map<String, String> map = new HashMap<>();
        map.put("code", code);

        List<Professor> l = dao.consultaSimples("consulta.prof", map);
        
        return l.get(0);
        
    }
}
