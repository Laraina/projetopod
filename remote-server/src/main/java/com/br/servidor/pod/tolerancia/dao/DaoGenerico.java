/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.servidor.pod.tolerancia.dao;

import com.br.servidor.pod.tolerancia.interfaces.InterfaceDao;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TransactionRequiredException;

/**
 *
 * @author Felipe
 */
public class DaoGenerico implements InterfaceDao {

    private EntityManager em;

    public DaoGenerico() {
    }

    @Override
    public boolean persist(Object t) {
        try {
            open();
            em.getTransaction().begin();
            em.persist(t);
            em.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | TransactionRequiredException | EntityExistsException e) {
            e.printStackTrace();
            em.getTransaction().rollback();
            em.close();
            return false;
        }
    }

    @Override
    public boolean update(Object t) {
        try {
            open();
            em.getTransaction().begin();
            em.merge(t);
            em.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | TransactionRequiredException e) {
            em.getTransaction().rollback();
            em.close();
            return false;
        }
    }

    @Override
    public boolean delete(Object t) {
        try {
            open();
            em.getTransaction().begin();
            em.remove(em.merge(t));
            em.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | TransactionRequiredException e) {
            em.getTransaction().rollback();
            em.close();
            return false;
        }
    }

    @Override
    public Object find(int id, Class c) {
        try {
            open();
            em.getTransaction().begin();
            Object o = em.find(c, id);
            em.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | TransactionRequiredException e) {
            em.getTransaction().rollback();
            em.close();
            return null;
        }
    }

    private void open() {
        em = Persistence.createEntityManagerFactory("persistence-pod").createEntityManager();
    }

    @Override
    public List listar(String sql) {
        try {
            open();
            Query q = em.createNamedQuery(sql);
            return q.getResultList();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List consultaSimples(String sql, Map<?, ?> map) {
       
        open();
        
        Query q = em.createNamedQuery(sql);
        
        for(String key : (Set<String>) map.keySet()){
            q.setParameter(key, (String)map.get(key));
        }
        
        return q.getResultList();
        
    }
}
