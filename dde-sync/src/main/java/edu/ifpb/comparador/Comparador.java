package edu.ifpb.comparador;

import br.com.dde.entidades.Course;
import br.com.dde.entidades.Teacher;
import com.br.interfaces.beans.Curso;
import com.br.interfaces.beans.Professor;
import edu.ifpb.hash.GerarListas;
import edu.ifpb.hash.HashCalc;
import edu.ifpb.persistencia.GerenciadorPersistencia;
import edu.ifpb.sincronizacao.RepositorioCurso;
import edu.ifpb.sincronizacao.RepositorioProfessor;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Comparador {

    private List<Course> diffCurso;
    private List<Teacher> diffProfessor;
    private GerenciadorPersistencia persistence;

    public Comparador() {
    }

    public void comparaHashProfessor() {
        try {

            persistence = GerenciadorPersistencia.getInstance();
            
            RepositorioProfessor rep = new RepositorioProfessor();
            List<Teacher> c1 = rep.getProfessoresB1();
            List<Professor> c2 = rep.getProfessoresB2();

            GerarListas g = new GerarListas();
            List<String> lista1 = g.concatenaInformacoesTeacher(c1);
            List<String> lista2 = g.concatenaInformacoesProfessor(c2);

            HashCalc hashCalc = new HashCalc();
            List<String> hash1 = hashCalc.calculaHash(lista1);
            List<String> hash2 = hashCalc.calculaHash(lista2);

            int count = 0;
            diffProfessor = new ArrayList<>();

            for (String h1 : hash1) {

                if (hash2.isEmpty()) {
                    diffProfessor.add(c1.get(count));
                } else {
                    int countAux = 0;

                    for (String h2 : hash2) {
                        if (!h1.equals(h2)) {
                            ++countAux;
                        }
                    }

                    if (countAux == hash2.size()) {
                        diffProfessor.add(c1.get(count));
                    }
                }
                count++;
            }
            
            persistence.setListAlteraProfessor(diffProfessor);

        } catch (NoSuchAlgorithmException | NotBoundException ex) {
            ex.printStackTrace();
        } catch (RemoteException ex) {
            System.out.println("Sem conexão! Gravando dados no arquivo");    
            persistence.setListAlteraProfessor( new ArrayList<Teacher>() );
        } 

    }

    public void comparaHashCurso(){
        try {
            
            persistence = GerenciadorPersistencia.getInstance();
            
            RepositorioCurso rep = new RepositorioCurso();
            List<Course> c1 = rep.getCursosB1();
            List<Curso> c2 = rep.getCursosB2();

            GerarListas g = new GerarListas();
            List<String> lista1 = g.concatenaInformacoesCourse(c1);
            List<String> lista2 = g.concatenaInformacoesCurso(c2);

            HashCalc hashCalc = new HashCalc();
            List<String> hash1 = hashCalc.calculaHash(lista1);
            List<String> hash2 = hashCalc.calculaHash(lista2);

            int count = 0;

            diffCurso = new ArrayList<>();
            for (String h1 : hash1) {

                if (hash2.isEmpty()) {
                    diffCurso.add(c1.get(count));
                } else {

                    int countAux = 0;

                    for (String h2 : hash2) {
                        if (!h1.equals(h2)) {
                            ++countAux;
                        }
                    }

                    if (countAux == hash2.size()) {
                        diffCurso.add(c1.get(count));
                    }

                }
                count++;
            }

            persistence.setListAlteraCurso(diffCurso);
            
        } catch (NoSuchAlgorithmException | NotBoundException ex) {
            ex.printStackTrace();
        } catch (RemoteException ex) {
            System.out.println("Sem conexão! Gravando dados no arquivo");    
            persistence.setListAlteraCurso( new ArrayList<Course>() );
        }     
    }
    
}
