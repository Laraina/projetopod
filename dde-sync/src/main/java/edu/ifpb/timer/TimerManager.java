/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.timer;

import edu.ifpb.comparador.Comparador;
import edu.ifpb.csv.CsvManager;
import edu.ifpb.persistencia.GerenciadorPersistencia;
import java.util.TimerTask;

/**
 *
 * @author Laraina
 */
public class TimerManager extends TimerTask {

    private GerenciadorPersistencia gerenciador;

    @Override
    public void run() {

        CsvManager csv = new CsvManager();
        Comparador comparador = new Comparador();

        if (csv.arquivoEstaVazio("dadosProfessor.csv")) {
            comparador.comparaHashProfessor();
        }

        if (csv.arquivoEstaVazio("dadosCurso.csv")) {
            comparador.comparaHashCurso();
        }

        gerenciador = GerenciadorPersistencia.getInstance();

        if (!csv.arquivoEstaVazio("dadosProfessor.csv")) {
            gerenciador.enviaProfessor();
        }

        if (!csv.arquivoEstaVazio("dadosCurso.csv")) {
            gerenciador.enviaCurso();
        }

    }
}
