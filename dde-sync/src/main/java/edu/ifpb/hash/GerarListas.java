package edu.ifpb.hash;


import br.com.dde.entidades.Course;
import br.com.dde.entidades.Teacher;
import com.br.interfaces.beans.Curso;
import com.br.interfaces.beans.Professor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fatinha de Sousa
 */

public class GerarListas {

    private List<String> informacoes;
    private StringBuilder builder;
    
    public List<String> concatenaInformacoesTeacher(List<Teacher> teachers){
        
        informacoes = new ArrayList();
        
        for(Teacher t: teachers){
            String temp = t.getCode()+""+t.getName()+""+t.getAbreviation()+""+t.getEmail()+""+t.getPhone();
            informacoes.add(temp);
        }
        
        return informacoes;
    }
    
    public List<String> concatenaInformacoesCourse(List<Course> courses){
        
        informacoes = new ArrayList();
        
        for(Course c: courses){
            String temp = c.getCode()+""+c.getName();
            informacoes.add(temp);
        }
        
        return informacoes;
    }
    
    public List<String> concatenaInformacoesProfessor(List<Professor> lista){
        informacoes = new ArrayList<>();
        
        for(Professor p : lista){
            builder = new StringBuilder();
            builder.append(p.getCode()).append(p.getNome()).append(p.getAbreviacao()).append(p.getEmail()).append(p.getTelefone());
            informacoes.add(builder.toString());
        }
        return informacoes;
    }
    
    public List<String> concatenaInformacoesCurso(List<Curso> lista){
        informacoes = new ArrayList<>();
        
        for(Curso c : lista){
            builder = new StringBuilder();
            builder.append(c.getCode()).append(c.getDescricao());
            informacoes.add(builder.toString());
        }
        
        return informacoes;
    }
    
}