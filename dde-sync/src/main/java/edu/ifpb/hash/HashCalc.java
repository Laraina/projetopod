package edu.ifpb.hash;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class HashCalc {

    public HashCalc() {
    }

   public List<String> calculaHash(List<String> objetos) throws NoSuchAlgorithmException {
       
        List<String> hash = new ArrayList();

        try {
            for (String nome : objetos) {
                hash.add(gerarHash(nome));
            }
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        
        return hash;
    }

    public String gerarHash(String nome) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        
        MessageDigest msg = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, msg.digest(nome.getBytes("UTF-8")));
        
        return hash.toString(16);
        
    }
}
