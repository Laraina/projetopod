package edu.ifpb.sincronizacao;

import br.com.dde.entidades.Teacher;
import br.com.dde.fachada.InterfaceFachada;
import com.br.interfaces.beans.Professor;
import com.br.interfaces.pod.InterfaceFachadaMySQL;
import edu.ifpb.rmi.Gerenciador;
import edu.ifpb.rmi.GerenciadorRemoteMySQL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

public class RepositorioProfessor {

    private InterfaceFachada inter;
    private InterfaceFachadaMySQL mysql;

    public RepositorioProfessor() throws RemoteException, NotBoundException {
        inter = Gerenciador.init();
        mysql = GerenciadorRemoteMySQL.init();
    }

    public List<Teacher> getProfessoresB1() throws RemoteException, NotBoundException {
        return inter.buscarTodosProfessores();
    }

    // recuperar do banco de felipe
    public List<Professor> getProfessoresB2() throws RemoteException {
        return mysql.listarProfessor();
    }
    
}
