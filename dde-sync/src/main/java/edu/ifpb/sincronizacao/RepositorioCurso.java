package edu.ifpb.sincronizacao;

import br.com.dde.entidades.Course;
import br.com.dde.fachada.InterfaceFachada;
import com.br.interfaces.beans.Curso;
import com.br.interfaces.pod.InterfaceFachadaMySQL;
import edu.ifpb.rmi.GerenciadorRemoteMySQL;
import edu.ifpb.rmi.Gerenciador;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.util.List;

public class RepositorioCurso {

    private InterfaceFachada inter;
    private InterfaceFachadaMySQL mysql;

    public RepositorioCurso() throws RemoteException, NotBoundException {
        inter = Gerenciador.init();
        mysql = GerenciadorRemoteMySQL.init();
    } 
    
    //banco de fatinha
    public List<Course> getCursosB1() throws RemoteException, NotBoundException {
        
        return inter.buscarTodosCurso();

    }

    // recuperar do banco de felipe
    public List<Curso> getCursosB2() throws RemoteException {
        return mysql.listarCursos();
    }
    
}
