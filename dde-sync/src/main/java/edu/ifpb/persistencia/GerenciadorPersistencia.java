package edu.ifpb.persistencia;

import br.com.dde.entidades.Course;
import br.com.dde.entidades.Teacher;
import com.br.interfaces.beans.Curso;
import com.br.interfaces.beans.Professor;
import com.br.interfaces.beans.Unidade;
import com.br.interfaces.pod.InterfaceFachadaMySQL;
import edu.ifpb.csv.CsvManager;
import edu.ifpb.rmi.GerenciadorRemoteMySQL;
import java.io.FileNotFoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GerenciadorPersistencia {

    public static GerenciadorPersistencia instance = null;
    private List<Teacher> professores;
    private List<Course> cursos;
    private CsvManager csv;
    private InterfaceFachadaMySQL fachada;

    private GerenciadorPersistencia() {
        this.professores = new ArrayList<>();
        this.cursos = new ArrayList<>();
        this.csv = new CsvManager();
    }

    public static GerenciadorPersistencia getInstance() {
        if (instance == null) {
            return instance = new GerenciadorPersistencia();
        }
        return instance;
    }

    public void setListAlteraProfessor(List<Teacher> professores) {

        for (Teacher t : professores) {
            csv.escreveArquivoProfessor(t);
        }

    }

    public void setListAlteraCurso(List<Course> cursos) {

        for (Course c : cursos) {
            csv.escreveArquivoCurso(c);
        }

    }

    public void enviaProfessor() {
        try {

            professores = csv.lerArquivoTeacher();

            fachada = GerenciadorRemoteMySQL.init();

            if (!professores.isEmpty()) {

                for (Iterator<Teacher> t = professores.iterator(); t.hasNext();) {

                    Teacher t1 = t.next();

                    if (!fachada.existeProfessor(t1.getCode())) {

                        boolean result = fachada.cadastrarProfessor(getProfessor(t1));

                        if (result) {
                            t.remove();
                        }

                    } else {

                        Professor p1 = getProfessor(t1);
                        Professor aux = fachada.buscarProfessor(p1.getCode());
                        aux.setNome(p1.getNome());
                        aux.setAbreviacao(p1.getAbreviacao());
                        aux.setEmail(p1.getEmail());
                        aux.setTelefone(p1.getTelefone());
                        aux.setAtivo(p1.getAtivo());

                        boolean result = fachada.atualizarProfessor(aux);

                        if (result) {
                            t.remove();
                        }
                    }
                }

                csv.limpandoArquivo("dadosProfessor.csv");

            }

        } catch (FileNotFoundException | NotBoundException ex) {
            ex.printStackTrace();
        } catch (RemoteException ex) {
            System.out.println("Sem conexão! Gravando dados no arquivo");    
            csv.limpandoArquivo("dadosProfessor.csv");
            for (Teacher t : professores) {
                csv.escreveArquivoProfessor(t);
            }
        }
    }

    public void enviaCurso() {
        try {

            cursos = csv.lerArquivoCurso();

            fachada = GerenciadorRemoteMySQL.init();

            if (!cursos.isEmpty()) {

                for (Iterator<Course> c = cursos.iterator(); c.hasNext();) {

                    Course c1 = c.next();

                    if (!fachada.existeCurso(c1.getCode())) {

                        Curso curso = getCurso(c1);

                        boolean result = fachada.cadastrarCurso(curso);

                        if (result) {
                            c.remove();
                        }

                    } else {

                        Curso cursoAux = getCurso(c1);

                        Curso aux = fachada.buscarCurso(cursoAux.getCode());

                        aux.setDescricao(cursoAux.getDescricao());

                        boolean result = fachada.atualizarCurso(aux);

                        if (result) {
                            c.remove();
                        }
                    }
                }

                csv.limpandoArquivo("dadosCurso.csv");
            }

        } catch (FileNotFoundException | NotBoundException ex) {
            ex.printStackTrace();
        } catch (RemoteException ex) {
            System.out.println("Sem conexão! Gravando dados no arquivo");    
            csv.limpandoArquivo("dadosCurso.csv");
            for (Course c : cursos) {
                csv.escreveArquivoCurso(c);
            }
        }
    }

    private Professor getProfessor(Teacher t) throws RemoteException {

        //qualquer erro, olhar novamente essa parte
        Unidade u = new Unidade(Integer.parseInt(t.getDepartment().getCode()), t.getDepartment().getName());

        boolean ativo = false;

        if (t.getStatus().equals("true")) {
            ativo = true;
        }

        return new Professor(t.getCode(), t.getName(), t.getAbreviation(), u, ativo, t.getEmail(), t.getPhone());

    }

    public Curso getCurso(Course c) throws RemoteException {

        Unidade u = new Unidade(2, "UNIDADE DE INDUSTRIA");
        return new Curso(c.getCode(), c.getName(), u);

    }
    
}
