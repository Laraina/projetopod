/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.rmi;

import com.br.interfaces.pod.InterfaceFachadaMySQL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author Felipe
 */
public class GerenciadorRemoteMySQL {

    public static InterfaceFachadaMySQL init() throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost", 10909);
        InterfaceFachadaMySQL fachada = (InterfaceFachadaMySQL) registry.lookup("fachadaJNDI");
        return fachada;
    }
}
