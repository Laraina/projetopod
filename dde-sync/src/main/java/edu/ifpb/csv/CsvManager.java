package edu.ifpb.csv;

import br.com.dde.entidades.Course;
import br.com.dde.entidades.Department;
import br.com.dde.entidades.Teacher;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CsvManager {

    private ArrayList<String> dados;
    private File file;
    private FileWriter myWriter;
    private Scanner input;
    private Scanner linha;
    private List<Course> listaCourse;
    private List<Teacher> listaTeacher;

    public CsvManager() {
        criarArquivo("dadosProfessor.csv");
        criarArquivo("dadosCurso.csv");
    }

    /**
     * Método que cria um arquivo CSV
     */
    private void criarArquivo( String nome ) {
        file = new File( nome );
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Método que recebe um Teacher e grava os valores de seus atributos em um
     * arquivo CSV
     *
     * @param t
     */
    public void escreveArquivoProfessor(Teacher t) {
        try {

            myWriter = new FileWriter("dadosProfessor.csv", true);

            myWriter.append(t.getCode());
            myWriter.append(',');
            myWriter.append(t.getName());
            myWriter.append(',');
            myWriter.append(t.getAbreviation());
            myWriter.append(',');
            myWriter.append(t.getEmail());
            myWriter.append(',');
            myWriter.append(t.getDepartment().getCode());
            myWriter.append(',');
            myWriter.append(t.getDepartment().getName());
            myWriter.append(',');
            myWriter.append(t.getPhone());
            myWriter.append(',');
            myWriter.append(t.getStatus());
            myWriter.append("\n");

            myWriter.flush();
            myWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que recebe um Course e grava os valores de seus atributos em um
     * arquivo CSV
     *
     * @param c
     */
    public void escreveArquivoCurso(Course c) {
        try {

            myWriter = new FileWriter("dadosCurso.csv", true);

            myWriter.append(c.getCode());
            myWriter.append(',');
            myWriter.append(c.getName());
            myWriter.append("\n");

            myWriter.flush();
            myWriter.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Método que ler um arquivo CSV e retorna uma listaCourse de Course. Caso o
     * arquivo não exista retorna null.
     *
     * @return List<Course>
     * @throws FileNotFoundException
     */
    public List<Course> lerArquivoCurso() throws FileNotFoundException {

        file = new File("dadosCurso.csv");

        if (file.exists()) {

            input = new Scanner(file);

            listaCourse = new ArrayList<>();
            dados = new ArrayList<>();

            while (input.hasNextLine()) {

                String line = input.nextLine().trim();

                if (!line.equals("") && !line.isEmpty()) {
                    linha = new Scanner(line);
                    linha.useDelimiter(",");

                    while (linha.hasNext()) {
                        dados.add(linha.next());
                    }

                    Course c = new Course();

                    c.setCode(dados.get(0));
                    c.setName(dados.get(1));
                    listaCourse.add(c);
                    dados.clear();
                }

            }

            linha.close();
            input.close();

            return listaCourse;
        }
        return null;
    }

    /**
     * Método que ler um arquivo CSV e retorna uma listaTeacher de Teacher. Caso
     * o arquivo não exista retorna null.
     *
     * @return List<Teacher>
     * @throws FileNotFoundException
     */
    public List<Teacher> lerArquivoTeacher() throws FileNotFoundException {

        file = new File("dadosProfessor.csv");

        if (file.exists()) {

            input = new Scanner(file);

            listaTeacher = new ArrayList<>();
            dados = new ArrayList<>();

            while (input.hasNextLine()) {

                String line = input.nextLine().trim();

                if (!line.equals("") && !line.isEmpty()) {

                    linha = new Scanner(line);
                    linha.useDelimiter(", *");

                    while (linha.hasNext()) {
                        dados.add(linha.next());
                    }

                    Teacher t = new Teacher();

                    t.setCode(dados.get(0));
                    t.setName(dados.get(1));
                    t.setAbreviation(dados.get(2));
                    t.setEmail(dados.get(3));
                    Department d = new Department(dados.get(4), dados.get(5));
                    t.setDepartment(d);
                    t.setPhone(dados.get(6));
                    t.setStatus(dados.get(7));

                    listaTeacher.add(t);
                    dados.clear();

                }

            }

            linha.close();
            input.close();

            return listaTeacher;
        }
        return null;
    }

    /**
     * Método que exclui o arquivo CSV e cria um arquivo novo
     */
    public void limpandoArquivo( String nome ) {
        file = new File( nome );
        if (file.exists()) {
            try {
                myWriter = new FileWriter(file);
                myWriter.append("");
                myWriter.flush();
                myWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Método que verifica se o arquivo está vazio. Caso não exista linhas no arquivo ele retorna true, caso contrário
     * ele retorna false
     * @return boolean
     */
    public boolean arquivoEstaVazio( String nome ){
        file = new File( nome );
        if (file.exists()) {
            try {
                input = new Scanner(file);
                return !input.hasNextLine();   
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }   
        }
        return false;
    }
    
}
