/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.pod;

import com.br.interfaces.beans.Curso;
import com.br.interfaces.beans.Nivel;
import com.br.interfaces.beans.Professor;
import com.br.interfaces.beans.Regime;
import com.br.interfaces.beans.Turno;
import com.br.interfaces.beans.Unidade;
import com.br.interfaces.beans.Vinculo;
import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceFachadaMySQL extends Serializable, Remote{
    
    //métodos de cadastrar
    public boolean cadastrarCurso(Curso c)throws RemoteException;
    public boolean cadastrarProfessor(Professor p)throws RemoteException;
    public boolean atualizarCurso(Curso c)throws RemoteException;
    public boolean atualizarProfessor(Professor p)throws RemoteException;
    public List<Professor> listarProfessor()throws RemoteException;
    public List<Curso> listarCursos()throws RemoteException;
    public boolean  existeProfessor(String code)throws RemoteException;
    public boolean  existeCurso(String code)throws RemoteException;
    public Turno buscarTurno(int id)throws RemoteException;
    public Unidade buscarUnidade(int id)throws RemoteException;
    public Nivel buscarNivel(int id)throws RemoteException;
    public Regime buscarRegime(int id)throws RemoteException;
    public Vinculo buscarVinculo(int id)throws RemoteException;
    public Curso buscarCurso(String code)throws RemoteException;
    public Professor buscarProfessor(String code)throws RemoteException;
        
//    public boolean cadastrarDisciplina(Disciplina disc)throws RemoteException;
//    public boolean cadastrarDisponibilidade(Disponibilidade disp)throws RemoteException;
//    public boolean cadastrarNivel(Nivel n)throws RemoteException;
//    public boolean cadastrarRegime(Regime r)throws RemoteException;
//    public boolean cadastrarSemestre(Semestre s)throws RemoteException;
//    public boolean cadastrarTurno(Turno t)throws RemoteException;
//    public boolean cadastrarUnidade(Unidade u)throws RemoteException;
//    public boolean cadastrarVinculo(Vinculo v)throws RemoteException;
    
    //métodos de atualizar
//    public boolean atualizarDisciplina(Disciplina disc)throws RemoteException;
//    public boolean atualizarDisponibilidade(Disponibilidade disp)throws RemoteException;
//    public boolean atualizarNivel(Nivel n)throws RemoteException;
//    public boolean atualizarRegime(Regime r)throws RemoteException;
//    public boolean atualizarSemestre(Semestre s)throws RemoteException;
//    public boolean atualizarTurno(Turno t)throws RemoteException;
//    public boolean atualizarUnidade(Unidade u)throws RemoteException;
//    public boolean atualizarVinculo(Vinculo v)throws RemoteException;
    
    //métodos de excluir
//    public boolean removerDisponibilidade(int id)throws RemoteException;

    
    //métodos de buscar
//    public Disciplina buscarDisciplina(int id)throws RemoteException;
//    public Disponibilidade buscarDisponibilidade(int id)throws RemoteException;
//    public Semestre buscarSemestre(int id)throws RemoteException;
        
}
