/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "disciplina")
@SequenceGenerator(name = "disciplina_codigo_seq", allocationSize = 1, initialValue = 1)
public class Disciplina implements Serializable{

    @Id
    @GeneratedValue(generator = "disciplina_codigo_seq", strategy = GenerationType.SEQUENCE)
    private int codigo;
    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "curso", referencedColumnName = "codigo")
    private Curso curso;
    @Column(length = 42)
    private String descricao;
    @Column(length = 14)
    private String abreviacao;
    private int periodo;
    private int carga_horaria;
    private int aulas_semana;

    public Disciplina() {
    }

    public Disciplina(Curso curso, String descricao, String abreviacao, int periodo, int carga_horaria, int aulas_semana) {
        this.curso = curso;
        this.descricao = descricao;
        this.abreviacao = abreviacao;
        this.periodo = periodo;
        this.carga_horaria = carga_horaria;
        this.aulas_semana = aulas_semana;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getCarga_horaria() {
        return carga_horaria;
    }

    public void setCarga_horaria(int carga_horaria) {
        this.carga_horaria = carga_horaria;
    }

    public int getAulas_semana() {
        return aulas_semana;
    }

    public void setAulas_semana(int aulas_semana) {
        this.aulas_semana = aulas_semana;
    }
}
