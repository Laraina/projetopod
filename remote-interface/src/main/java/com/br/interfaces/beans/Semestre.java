/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "semestre")
@SequenceGenerator(name = "semestre_codigo_seq", allocationSize = 1, initialValue = 1)
public class Semestre implements Serializable{

    @Id
    @GeneratedValue(generator = "semestre_codigo_seq",strategy = GenerationType.SEQUENCE)
    private int codigo;
    private int ano;
    private int periodo;
    @Column(nullable = false)
    private boolean ativo;

    public Semestre() {
    }

    public Semestre(int ano, int periodo, boolean ativo) {
        this.ano = ano;
        this.periodo = periodo;
        this.ativo = ativo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
