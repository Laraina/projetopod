/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "professor")
@SequenceGenerator(name = "professor_codigo_seq", allocationSize = 1, initialValue = 1)
@NamedQueries(value = {
    @NamedQuery(name = "listar.prof", query = "SELECT p FROM Professor p ORDER BY p.nome"),
    @NamedQuery(name = "consulta.prof", query = "SELECT p FROM Professor p WHERE p.code = :code")
})
public class Professor implements Serializable{

    @Id
    @GeneratedValue(generator = "professor_codigo_seq", strategy = GenerationType.SEQUENCE)
    private int codigo;
    private String code;
    @Column(length = 40)
    private String nome;
    @Column(length = 14)
    private String abreviacao;
    @OneToOne
    @JoinColumn(name = "unidade", referencedColumnName = "codigo")
    private Unidade unidade;
    @OneToOne
    @JoinColumn(name = "vinculo", referencedColumnName = "codigo")
    private Vinculo vinculo;
    @OneToOne
    @JoinColumn(name = "regime", referencedColumnName = "codigo")
    private Regime regime;
    private boolean ativo;
    @Column(length = 200)
    private String email;
    @Column(length = 50)
    private String telefone;

    public Professor() {        
        
    }

    public Professor(String code, String nome, String abreviacao, Unidade unidade, boolean ativo, String email, String telefone) {
        this.code = code;
        this.nome = nome;
        this.abreviacao = abreviacao;
        this.unidade = unidade;
        this.vinculo = new Vinculo(33, "nulo");
        this.regime = new Regime(3, "n");
        this.ativo = ativo;
        this.email = email;
        this.telefone = telefone;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public Vinculo getVinculo() {
        return vinculo;
    }

    public void setVinculo(Vinculo vinculo) {
        this.vinculo = vinculo;
    }

    public Regime getRegime() {
        return regime;
    }

    public void setRegime(Regime regime) {
        this.regime = regime;
    }

    public boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
