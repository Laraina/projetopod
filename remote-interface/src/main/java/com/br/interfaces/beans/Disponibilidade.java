/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "disponibilidade")
@SequenceGenerator(name = "disponibilidade_codigo_seq", allocationSize = 1, initialValue = 1)
public class Disponibilidade implements Serializable{

    @Id
    @GeneratedValue(generator = "disponibilidade_codigo_seq", strategy = GenerationType.SEQUENCE)
    private int codigo;
    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "professor", referencedColumnName = "codigo")
    private Professor professor;
    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "semestre", referencedColumnName = "codigo")
    private Semestre semestre;
    private boolean disp_segunda;
    private boolean disp_terca;
    private boolean disp_quarta;
    private boolean disp_quinta;
    private boolean disp_sexta;
    @Column(length = 1000)
    private String restricao;

    public Disponibilidade() {
    }

    public Disponibilidade(Professor professor, Semestre semestre, boolean disp_segunda, boolean disp_terca, boolean disp_quarta, boolean disp_quinta, boolean disp_sexta, String restricao) {
        this.professor = professor;
        this.semestre = semestre;
        this.disp_segunda = disp_segunda;
        this.disp_terca = disp_terca;
        this.disp_quarta = disp_quarta;
        this.disp_quinta = disp_quinta;
        this.disp_sexta = disp_sexta;
        this.restricao = restricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public boolean isDisp_segunda() {
        return disp_segunda;
    }

    public void setDisp_segunda(boolean disp_segunda) {
        this.disp_segunda = disp_segunda;
    }

    public boolean isDisp_terca() {
        return disp_terca;
    }

    public void setDisp_terca(boolean disp_terca) {
        this.disp_terca = disp_terca;
    }

    public boolean isDisp_quarta() {
        return disp_quarta;
    }

    public void setDisp_quarta(boolean disp_quarta) {
        this.disp_quarta = disp_quarta;
    }

    public boolean isDisp_quinta() {
        return disp_quinta;
    }

    public void setDisp_quinta(boolean disp_quinta) {
        this.disp_quinta = disp_quinta;
    }

    public boolean isDisp_sexta() {
        return disp_sexta;
    }

    public void setDisp_sexta(boolean disp_sexta) {
        this.disp_sexta = disp_sexta;
    }

    public String getRestricao() {
        return restricao;
    }

    public void setRestricao(String restricao) {
        this.restricao = restricao;
    }
    
}
