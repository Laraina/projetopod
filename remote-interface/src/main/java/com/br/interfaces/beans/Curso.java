/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "curso")
@SequenceGenerator(name = "curso_codigo_seq", allocationSize = 1, initialValue = 1)
@NamedQueries(value = {
    @NamedQuery(name = "listar.curso", query = "SELECT c FROM Curso c ORDER BY c.descricao"),
    @NamedQuery(name = "consultar.curso" , query = "SELECT c FROM Curso c WHERE c.code = :code")
})

public class Curso implements Serializable{

    @Id
    @GeneratedValue(generator = "curso_codigo_seq", strategy = GenerationType.SEQUENCE)
    private int codigo;
    private String code;
    @OneToOne
    @JoinColumn(name = "unidade", referencedColumnName = "codigo")
    private Unidade unidade;
    @Column(length = 40)
    private String descricao;
    @Column(length = 10)
    private String abreviacao;
    private int periodos;
    private int academico;
    @OneToOne
    @JoinColumn(name = "turno", referencedColumnName = "codigo")
    private Turno turno;
    @OneToOne
    @JoinColumn(name = "nivel", referencedColumnName = "codigo")
    private Nivel nivel;

    public Curso() {
    }

    public Curso(String code, String descricao, Unidade unidade) {
        this.code = code;
        this.unidade = unidade;
        this.descricao = descricao;
        this.abreviacao = "nulo";
        this.periodos = 1;
        this.academico = 1;
        this.turno = new Turno(1, "DIURNO", abreviacao);
        this.nivel = new Nivel(21, "Superior");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public int getPeriodos() {
        return periodos;
    }

    public void setPeriodos(int periodos) {
        this.periodos = periodos;
    }

    public int getAcademico() {
        return academico;
    }

    public void setAcademico(int academico) {
        this.academico = academico;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
}
