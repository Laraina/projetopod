/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "unidade")
@SequenceGenerator(name = "unidade_codigo_seq", allocationSize = 1, initialValue = 1)
public class Unidade implements Serializable{
    
    @Id
    @GeneratedValue(generator = "unidade_codigo_seq", strategy = GenerationType.SEQUENCE)
    private int codigo;
    @Column(length = 40)
    private String descricao;
    @Column(length = 10)
    private String abreviacao;

    
    public Unidade() {
    }
    
    public Unidade(int code, String descricao) {
        this.codigo = code;
        this.descricao = descricao;
        this.abreviacao = "nulo";
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }
    
}
