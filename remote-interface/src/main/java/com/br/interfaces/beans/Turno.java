/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.interfaces.beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "turno")
@SequenceGenerator(name = "turno_codigo_seq", allocationSize = 1, initialValue = 1)
public class Turno implements Serializable{

    @Id
    @GeneratedValue()
    private int codigo;
    @Column(length = 20)
    private String descricao;
    @Column(length = 4)
    private String abreviacao;

    public Turno() {
    }

    public Turno(int code, String descricao, String abreviacao) {
        this.codigo = code;
        this.descricao = descricao;
        this.abreviacao = abreviacao;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }
}
