/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacegrafica;

import br.com.dde.fachada.InterfaceFachada;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author Jannayna
 */
public class Gerenciador {

    public Gerenciador() {
    }
    
    public static InterfaceFachada init() throws RemoteException, NotBoundException{
        Registry registry = LocateRegistry.getRegistry("localhost", 20700);
        InterfaceFachada fachada = (InterfaceFachada) registry.lookup("FachadaJNDI");
        return fachada;
    }
}
