    /*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */
package interfacegrafica;

import br.com.dde.entidades.Course;
import br.com.dde.fachada.InterfaceFachada;
import java.awt.event.ActionEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 *
 *
 * @author Jannayna
 *
 */
public class TelaCurso extends javax.swing.JFrame {

    /**
     *
     * Creates new form telaCurso
     *
     */
    private static Course curso;
    private static InterfaceFachada inter;

    public TelaCurso() throws RemoteException, NotBoundException {
        initComponents();
        jUnidade.addItem("UNIND");
        jUnidade.addItem("UNINF");
        jUnidade.addItem("UFGP");
        inter = Gerenciador.init();
        jQtde.setText("");

        jProximo.setEnabled(false);
        jAnterior.setEnabled(false);
        jPrimeiro.setEnabled(false);
        jUltimo.setEnabled(false);

        jProximo.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jProximoActionPerformed(e);
            }
        });

        jAnterior.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jAnteriorActionPerformed(e);
            }
        });

        jPrimeiro.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPrimeiroActionPerformed(e);
            }
        });

        jUltimo.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jUltimoActionPerformed(e);
            }
        });

        jAdicionar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jAdicionarActionPerformed(e);
            }
        });

        jDeletar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jDeletarActionPerformed(e);
            }
        });

        jAtualizar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jAtualizarActionPerformed(e);
            }
        });

        jEditar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jEditarActionPerformed(e);
            }
        });

        jQtde.setText(String.valueOf(inter.buscarTodosCurso().size()));
    }

    public TelaCurso(Course c) throws RemoteException, NotBoundException {
        initComponents();
        jUnidade.addItem("UNIND");
        jUnidade.addItem("UNINF");
        jUnidade.addItem("UFGP");
        inter = Gerenciador.init();
        curso = c;
        jNome.setEditable(false);
        jAbreviacao.setEditable(false);
        jPeriodos.setEditable(false);
        jNome.setText(c.getName());
        jQtde.setText("");

        jProximo.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jProximoActionPerformed(e);
            }
        });

        jAnterior.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jAnteriorActionPerformed(e);
            }
        });

        jPrimeiro.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPrimeiroActionPerformed(e);
            }
        });

        jUltimo.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jUltimoActionPerformed(e);
            }
        });

        jAdicionar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jAdicionarActionPerformed(e);
            }
        });

        jDeletar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jDeletarActionPerformed(e);
            }
        });

        jAtualizar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jAtualizarActionPerformed(e);
            }
        });

        jEditar.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jEditarActionPerformed(e);
            }
        });

        jQtde.setText(String.valueOf(inter.buscarTodosCurso().size()));
    }

    private void jPrimeiroActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            List<Course> lista = inter.buscarTodosCurso();
            curso = lista.get(0);
            new TelaCurso(curso).setVisible(true);
            this.dispose();
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jUltimoActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            List<Course> lista = inter.buscarTodosCurso();
            curso = lista.get(lista.size() - 1);
            new TelaCurso(curso).setVisible(true);
            this.dispose();
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jProximoActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            List<Course> lista = inter.buscarTodosCurso();
            int index = 0;
            for (int i = 0; i < lista.size(); i++) {
                if (lista.get(i).getCode().equals(curso.getCode())) {
                    index = i;
                }
            }
            if (index != lista.size() - 1) {
                curso = lista.get(index + 1);
            }
            new TelaCurso(curso).setVisible(true);
            this.dispose();
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jAnteriorActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            List<Course> lista = inter.buscarTodosCurso();
            int index = 0;
            for (int i = 0; i < lista.size(); i++) {
                if (lista.get(i).getCode().equals(curso.getCode())) {
                    index = i;
                }
            }
            if (index != 0) {
                curso = lista.get(index - 1);
            }
            new TelaCurso(curso).setVisible(true);
            this.dispose();
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jAdicionarActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            new TelaCurso().setVisible(true);
            this.dispose();
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jEditarActionPerformed(java.awt.event.ActionEvent evt) {
        jNome.setEditable(true);
        jAbreviacao.setEditable(true);
        jPeriodos.setEditable(true);
    }

    private void jDeletarActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            if (inter.removerCurso(curso)) {
                JOptionPane.showMessageDialog(this, "Curso deletado com sucesso!");
                new TelaCurso().setVisible(true);
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Erro!");
            }
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jAtualizarActionPerformed(java.awt.event.ActionEvent evt) {
        if ((jNome.getText().length() <= 0) || (jAbreviacao.getText().length() <= 0) || (jPeriodos.getText().length() <= 0)) {
            JOptionPane.showMessageDialog(this, "Preencha todos os campos");
        } else {
            try {
                //String cod = "1";
                String nome = jNome.getText();
                String abreviacao = jAbreviacao.getText();
                String periodos = jPeriodos.getText();
                String unidade = jUnidade.getSelectedItem().toString();
                curso = new Course(curso.getCode(), nome);
                if (inter.atualizarCurso(curso)) {
                    JOptionPane.showMessageDialog(this, "Curso atualizado com sucesso!");
                } else {
                    JOptionPane.showMessageDialog(this, "Erro!");
                }
            } catch (RemoteException ex) {
                Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * This method is called from within the constructor to initialize the form.
     *
     * WARNING: Do NOT modify this code. The content of this method is always
     *
     * regenerated by the Form Editor.
     *
     */
    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jNome = new javax.swing.JTextField();
        jAbreviacao = new javax.swing.JTextField();
        jPeriodos = new javax.swing.JTextField();
        jUnidade = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableTotal = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableDisciplinas = new javax.swing.JTable();
        jQtde = new javax.swing.JLabel();
        jAtualizar = new javax.swing.JButton();
        jAdicionar = new javax.swing.JButton();
        jUltimo = new javax.swing.JButton();
        jProximo = new javax.swing.JButton();
        jAnterior = new javax.swing.JButton();
        jEditar = new javax.swing.JButton();
        jSalvar = new javax.swing.JButton();
        jDeletar = new javax.swing.JButton();
        jPrimeiro = new javax.swing.JButton();
        jProximoDis = new javax.swing.JButton();
        jAnteriorDis = new javax.swing.JButton();
        jEditarDis = new javax.swing.JButton();
        jSalvarDis = new javax.swing.JButton();
        jDeletarDis = new javax.swing.JButton();
        jPrimeiroDis = new javax.swing.JButton();
        jAtualizarDis = new javax.swing.JButton();
        jAdicionarDis = new javax.swing.JButton();
        jUltimoDis = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(903, 554));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Curso");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Nome");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Abreviação");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Unidade");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Períodos");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Quantidade:");

        jNome.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jNome.setForeground(new java.awt.Color(51, 51, 255));

        jAbreviacao.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jAbreviacao.setForeground(new java.awt.Color(51, 51, 255));

        jPeriodos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPeriodos.setForeground(new java.awt.Color(51, 51, 255));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Disciplinas");

        tableTotal.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{
                    {"Total", null, null, null}
                },
                new String[]{
                    "", "DISC", "C. HOR.", "AULAS"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableTotal);

        tableDisciplinas.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{
                    {null, null, null, null, null},
                    {null, null, null, null, null},
                    {null, null, null, null, null},
                    {null, null, null, null, null},
                    {null, null, null, null, null}
                },
                new String[]{
                    "Nome", "Abreviação", "Período", "C. Horária", "Aulas/Semana"
                }
        ));
        jScrollPane2.setViewportView(tableDisciplinas);

        jQtde.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jQtde.setText("18");

        jAtualizar.setText("Atualizar");

        jAdicionar.setText("+");

        jUltimo.setText(">|");

        jProximo.setText(">");

        jAnterior.setText("<");

        jEditar.setText("Editar");

        jSalvar.setText("Salvar");
        jSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jSalvarActionPerformed(evt);
            }
        });

        jDeletar.setText("Deletar");

        jPrimeiro.setText("|<");

        jProximoDis.setText(">");

        jAnteriorDis.setText("<");

        jEditarDis.setText("Editar");

        jSalvarDis.setText("Salvar");

        jDeletarDis.setText("Deletar");

        jPrimeiroDis.setText("|<");

        jAtualizarDis.setText("Atualizar");

        jAdicionarDis.setText("+");

        jUltimoDis.setText(">|");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jEditar)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(jSalvar)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jDeletar)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jAtualizar))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel2)
                                                        .addComponent(jNome, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(62, 62, 62)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel3)
                                                .addComponent(jAbreviacao, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(56, 56, 56)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel4)
                                                .addComponent(jUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 96, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jPeriodos, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                        .addComponent(jLabel5)
                                                        .addGap(18, 18, 18)))
                                        .addGap(37, 37, 37))))
                .addGroup(layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(jPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jUltimo, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jQtde)
                        .addGap(34, 34, 34))
                .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jEditarDis)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(jSalvarDis)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jDeletarDis)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jAtualizarDis))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jPrimeiroDis, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(jUltimoDis, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jAnteriorDis, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jProximoDis, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jAdicionarDis, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel7)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                                .addComponent(jSeparator1))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jAtualizar, jDeletar, jEditar, jSalvar});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel2)
                                                .addComponent(jLabel3)
                                                .addComponent(jLabel4))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jAbreviacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jNome, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jPeriodos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jQtde, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jAnterior)
                                .addComponent(jProximo)
                                .addComponent(jAdicionar)
                                .addComponent(jUltimo)
                                .addComponent(jPrimeiro))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jEditar)
                                .addComponent(jDeletar)
                                .addComponent(jAtualizar)
                                .addComponent(jSalvar))
                        .addGap(15, 15, 15)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jAnteriorDis)
                                                .addComponent(jProximoDis)
                                                .addComponent(jAdicionarDis)
                                                .addComponent(jUltimoDis)
                                                .addComponent(jPrimeiroDis))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jEditarDis)
                                                .addComponent(jDeletarDis)
                                                .addComponent(jAtualizarDis)
                                                .addComponent(jSalvarDis))))
                        .addContainerGap(121, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{jAbreviacao, jNome, jPeriodos, jUnidade});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{jAtualizar, jDeletar, jEditar, jSalvar});

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jSalvarActionPerformed

        if ((jNome.getText().length() <= 0) || (jAbreviacao.getText().length() <= 0) || (jPeriodos.getText().length() <= 0)) {
            JOptionPane.showMessageDialog(this, "Preencha todos os campos");
        } else {
            try {
                //String cod = "1";
                String nome = jNome.getText();
                String abreviacao = jAbreviacao.getText();
                String periodos = jPeriodos.getText();
                String unidade = jUnidade.getSelectedItem().toString();
                curso = new Course("", nome);
                if (inter.salvarCourse(curso)) {
                    JOptionPane.showMessageDialog(this, "Curso cadastrado com sucesso!");
                    jProximo.setEnabled(true);
                    jAnterior.setEnabled(true);
                    jPrimeiro.setEnabled(true);
                    jUltimo.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(this, "Erro!");
                }
            } catch (RemoteException ex) {
                Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jSalvarActionPerformed

    /**
     *
     * @param args the command line arguments
     *
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">

        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.

         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 

         */
        try {

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {

                if ("Nimbus".equals(info.getName())) {

                    javax.swing.UIManager.setLookAndFeel(info.getClassName());

                    break;

                }

            }

        } catch (ClassNotFoundException ex) {

            java.util.logging.Logger.getLogger(TelaCurso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {

            java.util.logging.Logger.getLogger(TelaCurso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {

            java.util.logging.Logger.getLogger(TelaCurso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

            java.util.logging.Logger.getLogger(TelaCurso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

        }

        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    inter = Gerenciador.init();
                    List<Course> lista = inter.buscarTodosCurso();
                    if (!lista.isEmpty()) {
                        curso = lista.get(0);
                        new TelaCurso(curso).setVisible(true);
                    } else {
                        new TelaCurso().setVisible(true);
                    }
                } catch (RemoteException | NotBoundException ex) {
                    Logger.getLogger(TelaCurso.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField jAbreviacao;
    private javax.swing.JButton jAdicionar;
    private javax.swing.JButton jAdicionarDis;
    private javax.swing.JButton jAnterior;
    private javax.swing.JButton jAnteriorDis;
    private javax.swing.JButton jAtualizar;
    private javax.swing.JButton jAtualizarDis;
    private javax.swing.JButton jDeletar;
    private javax.swing.JButton jDeletarDis;
    private javax.swing.JButton jEditar;
    private javax.swing.JButton jEditarDis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField jNome;
    private javax.swing.JTextField jPeriodos;
    private javax.swing.JButton jPrimeiro;
    private javax.swing.JButton jPrimeiroDis;
    private javax.swing.JButton jProximo;
    private javax.swing.JButton jProximoDis;
    private javax.swing.JLabel jQtde;
    private javax.swing.JButton jSalvar;
    private javax.swing.JButton jSalvarDis;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton jUltimo;
    private javax.swing.JButton jUltimoDis;
    private javax.swing.JComboBox jUnidade;
    private javax.swing.JTable tableDisciplinas;
    private javax.swing.JTable tableTotal;
    // End of variables declaration//GEN-END:variables

}
