package br.com.dde.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {

    private String url = "jdbc:postgresql://localhost:5432/dde-database";
    private String user = "postgres";
    private String password = "123456";
    private String driver = "org.postgresql.Driver";
    private static Connection conn;

    public Connect() throws ClassNotFoundException {

        Class.forName(driver);
        try {
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (conn == null) {
            new Connect();
        }
        return conn;
    }

    public static void closeConnection() throws SQLException {
        if (!conn.isClosed()) {
            conn.close();
        }
    }

}
