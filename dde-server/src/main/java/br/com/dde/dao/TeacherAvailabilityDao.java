package br.com.dde.dao;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.Teacher;
import br.com.dde.entidades.TeacherAvailability;
import br.com.dde.entidades.Version;
import br.com.dde.interfaces.InterfaceDaoTeacherAvailability;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fatinha de Sousa
 */
public class TeacherAvailabilityDao implements InterfaceDaoTeacherAvailability {

    @Override
    public void salvar(TeacherAvailability teacherAvailability) {
        VersionDao dao = new VersionDao();
        teacherAvailability.setVersion(dao.criarVersion());
        
        String sql = "insert into teacheravailability(flag_Monday, flag_Tuesday, flag_Wednesday, flag_Thursday, "
                + "flag_Friday, flag_Phding, flag_Mscing, version, teacher) values(cast(? as bit(1)), cast(? as bit(1)), "
                + "cast(? as bit(1)), cast(? as bit(1)), cast(? as bit(1)), cast(? as bit(1)), cast(? as bit(1)), ?, ?)";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setInt(1, teacherAvailability.getFlagMonday());
            pStm.setInt(2, teacherAvailability.getFlagTuesday());
            pStm.setInt(3, teacherAvailability.getFlagWednesday());
            pStm.setInt(4, teacherAvailability.getFlagThursday());
            pStm.setInt(5, teacherAvailability.getFlagFriday());
            pStm.setInt(6, teacherAvailability.getFlagPhding());
            pStm.setInt(7, teacherAvailability.getFlagMsing());
            pStm.setString(8, teacherAvailability.getVersion().getCode());
            pStm.setString(9, teacherAvailability.getTeacher().getCode());

            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherAvailabilityDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public boolean atualizar(TeacherAvailability teacherAvailability) {
        boolean status = false;
        VersionDao daoVersion = new VersionDao();
        teacherAvailability.setVersion(daoVersion.criarVersion());
        
        String sql = "insert into teacheravailability(flag_Monday, flag_Tuesday, flag_Wednesday, flag_Thursday, "
                + "flag_Friday, flag_Phding, flag_Mscing, version, teacher) values(cast(? as bit(1)), cast(? as bit(1)), "
                + "cast(? as bit(1)), cast(? as bit(1)), cast(? as bit(1)), cast(? as bit(1)), cast(? as bit(1)), ?, ?)";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setInt(1, teacherAvailability.getFlagMonday());
            pStm.setInt(2, teacherAvailability.getFlagTuesday());
            pStm.setInt(3, teacherAvailability.getFlagWednesday());
            pStm.setInt(4, teacherAvailability.getFlagThursday());
            pStm.setInt(5, teacherAvailability.getFlagFriday());
            pStm.setInt(6, teacherAvailability.getFlagPhding());
            pStm.setInt(7, teacherAvailability.getFlagMsing());
            pStm.setString(8, teacherAvailability.getVersion().getCode());
            pStm.setString(9, teacherAvailability.getTeacher().getCode());

            pStm.executeUpdate();
            pStm.close();
            
            status  = true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherAvailabilityDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    @Override
    public TeacherAvailability buscar(String teacher) {
        String sql = "select * from teacheravailability t where t.version = v.code and t.teacher = ?";
        TeacherAvailability teacherAvailability = new TeacherAvailability();
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacher);
            ResultSet rs = pStm.executeQuery();

            rs.close();
            pStm.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherAvailabilityDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return teacherAvailability;
    }

    @Override
    public boolean delete(TeacherAvailability teacherAvailability) {
        String sql = "delete from teacheravailability where teacher = ? and version = ?";
        boolean status = false;
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacherAvailability.getTeacher().getCode());
            pStm.setString(2, teacherAvailability.getVersion().getCode());
            pStm.executeUpdate();
            pStm.close();

            status = true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherAvailabilityDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    //Lista as disponibilidades de um professor no semestre corrente :)
    @Override
    public List<TeacherAvailability> listarDisponibilidade(String code) {
        List<TeacherAvailability> disponibilidade = new ArrayList();

        String sql = "select * from teacheravailability where teacher ilike ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            while (rs.next()) {
                TeacherAvailability teacherAvailability = new TeacherAvailability();
                Version v = new Version();
                v.setCode(rs.getString("version"));
                teacherAvailability.setVersion(v);

                Teacher teacher = new Teacher();
                teacher.setCode(rs.getString("teacher"));
                teacherAvailability.setTeacher(teacher);

                teacherAvailability.setFlagMonday(rs.getShort("flag_monday"));
                teacherAvailability.setFlagFriday(rs.getShort("flag_friday"));
                teacherAvailability.setFlagMsing(rs.getShort("flag_mscing"));
                teacherAvailability.setFlagPhding(rs.getShort("flag_phding"));
                teacherAvailability.setFlagThursday(rs.getShort("flag_thursday"));
                teacherAvailability.setFlagTuesday(rs.getShort("flag_tuesday"));
                teacherAvailability.setFlagWednesday(rs.getShort("flag_wednesday"));
                disponibilidade.add(teacherAvailability);
            }
            rs.close();
            pStm.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherAvailabilityDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return disponibilidade;
    }

    @Override
    public TeacherAvailability ultimaDispProfessor(String teacher, String version){
        List<TeacherAvailability> lista = listarDisponibilidade(teacher);
        
        TeacherAvailability teacherAvailability = lista.get(lista.size() - 1);
        
        return teacherAvailability;
    }
}
