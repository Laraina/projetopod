package br.com.dde.dao;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.TeacherCourseGrid;
import br.com.dde.interfaces.InterfaceDaoTeacherCourseGrid;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fatinha de Sousa
 */
public class TeacherCourseGridDao implements InterfaceDaoTeacherCourseGrid{

    public void salvar(TeacherCourseGrid teacherCourseGrid) {
        String sql = "insert into teacher_courseGrid(version, courseGrid, teacher) values (?, ?, ?)";
        
        try {
            PreparedStatement pStm;
            pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacherCourseGrid.getVersion().getCode());
            pStm.setString(2, teacherCourseGrid.getCourseGrid().getCode());
            pStm.setString(3, teacherCourseGrid.getTeacher().getCode());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherUserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizar(TeacherCourseGrid teacherCourseGrid) {
        String sql = "update teacher_courseGrid set version = ?, courseGrid = ?, teacher = ? where";
        
    }

    public TeacherCourseGrid buscar(String code) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void delete(TeacherCourseGrid teacherCourseGrid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
