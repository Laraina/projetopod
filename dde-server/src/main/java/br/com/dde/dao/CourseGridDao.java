package br.com.dde.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.Course;
import br.com.dde.entidades.CourseGrid;
import br.com.dde.interfaces.InterfaceDaoCourseGrid;

public class CourseGridDao implements InterfaceDaoCourseGrid {

    @Override
    public void salvar(CourseGrid courseGrid) {

        String sql = "insert into coursegrid(code, course, unitpart, unitname, unitcredits, unithours, status)"
                + "values(?, ?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, courseGrid.getCode());
            pStm.setString(2, courseGrid.getCourse().getCode());
            pStm.setInt(3, courseGrid.getUnitPart());
            pStm.setString(4, courseGrid.getUnitName());
            pStm.setInt(5, courseGrid.getUnitCredits());
            pStm.setInt(6, courseGrid.getUnitHours());
            pStm.setString(7, courseGrid.getStatus());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void atualizar(CourseGrid courseGrid) {

        String sql = "update coursegrid set course = ?, unitpart = ?, unitname = ?, unitcredits = ?, unitHours = ?,"
                + "status = ? where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, courseGrid.getCourse().getCode());
            pStm.setInt(2, courseGrid.getUnitPart());
            pStm.setString(3, courseGrid.getUnitName());
            pStm.setInt(4, courseGrid.getUnitCredits());
            pStm.setInt(5, courseGrid.getUnitHours());
            pStm.setString(6, courseGrid.getStatus());
            pStm.setString(7, courseGrid.getCode());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public CourseGrid buscar(String code) {

        String sql = "select * from coursegrid c where c.code = ?";
        CourseGrid courseGrid = new CourseGrid();
        
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            if (rs.next()) {
                courseGrid.setCode(rs.getString(1));
                
                Course course = new Course();
                course.setCode(rs.getString(2));
                courseGrid.setCourse(course);
                
                courseGrid.setUnitPart(rs.getInt(3));
                courseGrid.setUnitName(rs.getString(4));
                courseGrid.setUnitCredits(rs.getInt(5));
                courseGrid.setUnitHours(rs.getInt(6));
                courseGrid.setStatus(rs.getString(7));
                
            } else {
                courseGrid = null;
            }
            rs.close();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return courseGrid;
    }

    @Override
    public void delete(CourseGrid courseGrid) {

        String sql = "delete from coursegrid where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, courseGrid.getCode());
            pStm.executeUpdate();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
