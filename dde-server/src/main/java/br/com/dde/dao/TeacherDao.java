package br.com.dde.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.Department;
import br.com.dde.entidades.Teacher;
import br.com.dde.interfaces.InterfaceDaoTeacher;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TeacherDao implements InterfaceDaoTeacher {

    public String codigoProfessor() {
        List<Teacher> teachers = listarTodos();
        Teacher aux = new Teacher();
        int codigo = 1;
        if (!teachers.isEmpty()) {
            aux = teachers.get(teachers.size() - 1);
            codigo = Integer.parseInt(aux.getCode()) + 1;
        }

        return String.valueOf(codigo);
    }

    @Override
    public void salvar(Teacher teacher) {

        String sql = "insert into teacher(code, name, abbreviation, email, department, phone, status)"
                + "values(?, ?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement pStm;
            pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, codigoProfessor());
            pStm.setString(2, teacher.getName());
            pStm.setString(3, teacher.getAbreviation());
            pStm.setString(4, teacher.getEmail());
            pStm.setString(5, teacher.getDepartment().getCode());
            pStm.setString(6, teacher.getPhone());
            pStm.setString(7, teacher.getStatus());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean atualizar(Teacher teacher) {
        boolean status = false;
        String sql = "update teacher set name = ?, abbreviation = ?, email = ?, phone = ?, status = ?, department = ? where code = ?";
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacher.getName());
            pStm.setString(2, teacher.getAbreviation());
            pStm.setString(3, teacher.getEmail());
            pStm.setString(4, teacher.getPhone());
            pStm.setString(5, teacher.getStatus());
            pStm.setString(6, teacher.getDepartment().getCode());
            pStm.setString(7, teacher.getCode());

            pStm.executeUpdate();
            pStm.close();
            status = true;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public Teacher buscar(String code) {

        String sql = "select * from teacher t, department d where t.code = ?";
        Teacher teacher = new Teacher();
        //Department department = new Department();

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            if (rs.next()) {
                Department d = new Department();
                d.setCode(rs.getString(7));
                d.setName(rs.getString(8));
                teacher.setDepartment(d);

                teacher.setCode(rs.getString(1));
                teacher.setName(rs.getString(2));
                teacher.setAbreviation(rs.getString(3));
                teacher.setEmail(rs.getString(4));
                teacher.setPhone(rs.getString(5));
                teacher.setStatus(rs.getString(6));
            }
            rs.close();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return teacher;
    }

    @Override
    public boolean delete(Teacher teacher) {
        boolean status = false;

        String sql = "delete from teacher where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacher.getCode());
            pStm.executeUpdate();
            pStm.close();
            status = true;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return status;

    }

    @Override
    public List<Teacher> listarTodos() {
        String sql = "select * from teacher";
        List<Teacher> professores = new ArrayList();

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            ResultSet rs = pStm.executeQuery();

            while (rs.next()) {
                Teacher teacher = new Teacher();
                teacher.setCode(rs.getString(1));
                teacher.setName(rs.getString(2));
                teacher.setAbreviation(rs.getString(3));
                teacher.setEmail(rs.getString(4));

                Department department = new Department();
                department.setCode(rs.getString(5));
                teacher.setDepartment(department);

                teacher.setPhone(rs.getString(6));
                teacher.setStatus(rs.getString(7));

                professores.add(teacher);
            }

        } catch (SQLException ex) {
            Logger.getLogger(TeacherDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TeacherDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return professores;

    }

}
