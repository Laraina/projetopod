package br.com.dde.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.AppUser;
import br.com.dde.interfaces.InterfaceDaoAppUser;

public class AppUserDao implements InterfaceDaoAppUser {

    @Override
    public void salvar(AppUser appUser) {

        String sql = "insert into appUser(username, passkey, profile) values(?, ?, ?)";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, appUser.getUserName());
            pStm.setString(2, appUser.getPassKey());
            pStm.setString(3, appUser.getProfile());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void atualizar(AppUser appUser) {

        String sql = "update appUser set passkey = ?, profile = ? where username = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, appUser.getPassKey());
            pStm.setString(3, appUser.getProfile());
            pStm.setString(3, appUser.getUserName());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public AppUser buscar(String code) {

        String sql = "select * from appUser where username = ?";
        AppUser user = new AppUser();
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            if (rs.next()) {
                user.setUserName(rs.getString(1));
                user.setPassKey(rs.getString(2));
                user.setProfile(rs.getString(3));
            }
            rs.close();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public void delete(AppUser appUser) {

        String sql = "delete from appUser where username = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, appUser.getUserName());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
