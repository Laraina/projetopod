package br.com.dde.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.Department;
import br.com.dde.interfaces.InterfaceDaoDepartment;

public class DepartmentDao implements InterfaceDaoDepartment {

    @Override
    public void salvar(Department department) {

        String sql = "insert into department(code, name) values(?, ?)";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, department.getCode());
            pStm.setString(2, department.getName());
            pStm.executeUpdate();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void atualizar(Department department) {

        String sql = "update department set name = ? where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, department.getName());
            pStm.setString(2, department.getCode());
            pStm.executeUpdate();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Department buscar(String code) {

        String sql = "select * from department where code = ?";
        Department department = new Department();
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            if (rs.next()) {
                department.setCode(rs.getString(1));
                department.setName(rs.getString(2));
            }
            rs.close();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return department;
    }

    @Override
    public void delete(Department department) {

        String sql = "delete from department where code = ?";
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, department.getCode());
            pStm.executeUpdate();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
