package br.com.dde.dao;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.TeacherAvailability;
import br.com.dde.entidades.Version;
import br.com.dde.interfaces.InterfaceDaoVersion;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class VersionDao implements InterfaceDaoVersion {

    public Version criarVersion() {
        Version version = new Version();
        List<Version> todos = buscarTodos();

        int count;
        Version aux = new Version();
        
        if (todos.isEmpty()) {
            count = 1;
        } else {
            aux = todos.get(todos.size() - 1);
            count = (Integer.parseInt(aux.getCode())) + 1;
        }

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;

        version.setCode(String.valueOf(count));
        version.setYear(year);
        version.setFlagDefault(1);

        if (month <= 6) {
            version.setPart(1);
        } else {
            version.setPart(2);
        }

        aux.setFlagDefault(0);
        atualizar(aux);
        salvar(version); 
        return version;
    }

    public List<Version> buscarTodos() {
        String sql = "select * from Version";
        List<Version> versoesDisponiveis = new ArrayList();

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            ResultSet rs = pStm.executeQuery();

            while (rs.next()) {
                Version v = new Version();
                v.setCode(rs.getString("code"));
                v.setYear(rs.getInt("year"));
                v.setPart(rs.getInt("part"));
                v.setFlagDefault(rs.getInt("flag_default"));
                //v.setCreated((BigInteger) rs.getObject("created"));

                versoesDisponiveis.add(v);
            }
            rs.close();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return versoesDisponiveis;
    }

    @Override
    public void salvar(Version version) {

        String sql = "insert into version(code, year, part, flag_default, created) values(?, ?, ?, cast(? as bit(1)), ?)";

        PreparedStatement pStm;
        try {
            pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, version.getCode());
            pStm.setInt(2, version.getYear());
            pStm.setInt(3, version.getPart());
            pStm.setInt(4, version.getFlagDefault());
            pStm.setBigDecimal(5, BigDecimal.valueOf(new Date().getTime()));
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void atualizar(Version version) {

        String sql = "update version set year = ?, part = ?, flag_default = cast(? as bit(1)), created = ? where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setInt(1, version.getYear());
            pStm.setInt(2, version.getPart());
            pStm.setLong(3, version.getFlagDefault());
            pStm.setBigDecimal(4, BigDecimal.valueOf(new Date().getTime()));
            pStm.setString(5, version.getCode());
            pStm.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Version buscar(String code) {

        String sql = "select * from version where code = ?";
        Version version = new Version();
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            if (rs.next()) {
                version.setCode(rs.getString("code"));
                version.setYear(rs.getInt("year"));
                version.setPart(rs.getInt("part"));
                version.setFlagDefault(rs.getInt("flag_default"));
                //version.setCreated((BigInteger) rs.getObject("created"));
            }
            rs.close();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    @Override
    public void delete(Version version) {

        String sql = "delete from version where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, version.getCode());
            pStm.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void ultimaVersaoProfessor(String codigo) {
        TeacherAvailabilityDao dao = new TeacherAvailabilityDao();
        List<TeacherAvailability> disponidilidades = dao.listarDisponibilidade(codigo);

        Version aux = buscar(disponidilidades.get(disponidilidades.size() - 1).getVersion().getCode());
        aux.setFlagDefault(0);
        atualizar(aux);
    }
}
