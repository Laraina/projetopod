package br.com.dde.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.Course;
import br.com.dde.entidades.Teacher;
import br.com.dde.interfaces.InterfaceDaoCourse;
import java.util.ArrayList;
import java.util.List;

public class CourseDao implements InterfaceDaoCourse {

    public String codigoCurso() {
        List<Course> courses = buscarTodosCurso();
        Course aux = new Course();
        int codigo = 1;
        if (!courses.isEmpty()) {
            aux = courses.get(courses.size() - 1);
            codigo = Integer.parseInt(aux.getCode()) + 1;
        }

        return String.valueOf(codigo);
    }

    @Override
    public boolean salvar(Course cource) {
        boolean status = false;

        String sql = "insert into course(code, name) values(?, ?)";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, codigoCurso());
            pStm.setString(2, cource.getName());
            pStm.executeUpdate();
            pStm.close();
            status = true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public boolean update(Course course) {
        boolean status = false;

        String sql = "update course set name = ? where code = ?";

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, course.getName());
            pStm.setString(2, course.getCode());
            pStm.executeUpdate();
            pStm.close();
            status = true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public Course buscar(String code) {

        String sql = "select * from course where code = ?";
        Course course = new Course();
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, code);
            ResultSet rs = pStm.executeQuery();

            if (rs.next()) {
                course.setCode(rs.getString(1));
                course.setName(rs.getString(2));
            }
            rs.close();
            pStm.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return course;
    }

    @Override
    public boolean delete(Course course) {
        boolean status = false;

        String sql = "delete from course where code = ?";
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(
                    sql);
            pStm.setString(1, course.getCode());
            pStm.executeUpdate();
            pStm.close();
            status = true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return status;
    }

    @Override
    public List<Course> buscarTodosCurso() {
        String sql = "select * from Course";
        List<Course> cursos = new ArrayList();

        try {
            PreparedStatement stat = Connect.getConnection().prepareStatement(sql);
            ResultSet rs = stat.executeQuery();

            while (rs.next()) {
                Course course = new Course();
                course.setCode(rs.getString(1));
                course.setName(rs.getString(2));

                cursos.add(course);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return cursos;
    }

}
