package br.com.dde.dao;

import br.com.dde.connection.Connect;
import br.com.dde.entidades.TeacherUser;
import br.com.dde.interfaces.InterfaceDaoTeacherUser;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fatinha de Sousa
 */
public class TeacherUserDao implements InterfaceDaoTeacherUser {

    public void salvar(TeacherUser teacherUser) {
        String sql = "insert into teacher_User(teacher, appuser ) values (?, ?)";

        try {
            PreparedStatement pStm;
            pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacherUser.getTeacher().getCode());
            pStm.setString(2, teacherUser.getAppUser().getUserName());
            pStm.executeUpdate();
            pStm.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(TeacherUserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizar(TeacherUser teacherUser) {
        String sql = "update teacher_User set teacher = ?, appuser = ? where teacher = ? and ";
        //Pensando eh isso;
    }

    public TeacherUser buscar(String teacher, String appUser) {
        String sql = "select * from teacher_User where teacher = ? and appuser = ?";
        TeacherUser teacherUser = new TeacherUser();

        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacher);
            pStm.setString(2, appUser);
            ResultSet rs = pStm.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(TeacherUserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TeacherUserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return teacherUser;
    }

    public void delete(TeacherUser teacherUser) {
        String sql = "delete from teacher_user where teacher = ? and appuser = ?";
        try {
            PreparedStatement pStm = Connect.getConnection().prepareStatement(sql);
            pStm.setString(1, teacherUser.getTeacher().getCode());
            pStm.setString(2, teacherUser.getAppUser().getUserName());

            pStm.executeUpdate();
            pStm.close();

        } catch (SQLException ex) {
            Logger.getLogger(TeacherUserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TeacherUserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
