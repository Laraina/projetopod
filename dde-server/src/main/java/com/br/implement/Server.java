package com.br.implement;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author Fatinha de Sousa
 */

public class Server {

    public static void main(String [] args) throws RemoteException, AlreadyBoundException{
        System.out.println("INICIANDO SERVIDOR DA EQUIPE DE FATINHA)");
        
        Registry registry = LocateRegistry.createRegistry(20700);
        registry.bind("FachadaJNDI", new FachadaServer());
        System.out.println("Conexao Estabelecida com Sucesso");
    }
}
