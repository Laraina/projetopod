package com.br.implement;

import br.com.dde.dao.DepartmentDao;
import br.com.dde.dao.*;
import br.com.dde.entidades.*;
import br.com.dde.fachada.InterfaceFachada;
import br.com.dde.interfaces.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author Fatinha de Sousa
 */
public class FachadaServer extends UnicastRemoteObject implements InterfaceFachada {

    private InterfaceDaoAppUser daoAppUser;
    private InterfaceDaoCourse daoCourse;
    private InterfaceDaoTeacher daoTeacher;
    private InterfaceDaoDepartment daoDepartment;
    private InterfaceDaoCourseGrid daoCourseGrid;
    private InterfaceDaoTeacherAvailability daoTeacherAvailability;
    private InterfaceDaoTeacherUser daoTeacherUser;
    private InterfaceDaoVersion daoVersion;
    private InterfaceDaoTeacherCourseGrid daoTeacherCourseGrid;

    public FachadaServer() throws RemoteException {
        super();
        daoAppUser = new AppUserDao();
        daoCourse = new CourseDao();
        daoTeacher = new TeacherDao();
        daoDepartment = new DepartmentDao();
        daoCourseGrid = new CourseGridDao();
        daoTeacherAvailability = new TeacherAvailabilityDao();
        daoTeacherUser = new TeacherUserDao();
        daoVersion = new VersionDao();
        daoTeacherCourseGrid = new TeacherCourseGridDao();

    }

    @Override
    public String mensagemConfirmacao() throws RemoteException {
        return "Servidor: Ei, Okay!";
    }

    @Override
    public boolean salvarCourseGrid(CourseGrid course) throws RemoteException {
        if (course == null) {
            System.out.println(course.getCode());
            return false;
            //Manda uma mensagem de erro
        } else {
            //Chama o DAO
            daoCourseGrid.salvar(course);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }
    }

    @Override
    public boolean salvarAppUser(AppUser user) throws RemoteException {

        if (user == null) {
            return false;
            //Manda uma mensagem de erro
        } else {
            //Chama o DAO
            daoAppUser.salvar(user);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }
    }

    @Override
    public boolean salvarCourse(Course course) throws RemoteException {
        if (course == null) {
            return false;
            //Manda uma mensagem de erro
        } else {
            //Chama o DAO
            daoCourse.salvar(course);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }
    }

    @Override
    public boolean salvarDepartment(Department dep) throws RemoteException {
        if (dep == null) {
            System.out.println("entrou");
            return false;
            //Manda uma mensagem de erro
        } else {
            //Chama o DAO
            daoDepartment.salvar(dep);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }

    }

    @Override
    public boolean salvarTeacher(Teacher t) throws RemoteException {
        if (t == null){
            System.out.println("Erro! Codigo Jah Existe!");
            return false;
        } else {
            //Chama o DAO
            daoTeacher.salvar(t);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }
    }

    @Override
    public boolean salvarTeacherAvailability(TeacherAvailability teacherAvailability) throws RemoteException {
        if (teacherAvailability == null) {
            return false;
            //Manda uma mensagem de erro
        } else {
            //Chama o DAO
            daoTeacherAvailability.salvar(teacherAvailability);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }
    }

    @Override
    public boolean salvarTeacherCourseGrid(TeacherCourseGrid teacherCourseGrid) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean salvarTeacherUser(TeacherUser teacherUser) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean salvarVersion(Version version) throws RemoteException {
        if (version == null) {
            return false;
            //Manda uma mensagem de erro
        } else {
            //Chama o DAO
            daoVersion.salvar(version);
            System.out.println("Salvo Com Sucesso!");
            return true;
        }
    }

    @Override
    public Teacher buscarProfessor(String codigo) throws RemoteException {
        return daoTeacher.buscar(codigo);
    }

    /*buscar todos os cursos*/
    @Override
    public List<Course> buscarTodosCurso() throws RemoteException {
        return daoCourse.buscarTodosCurso();
    }

    @Override
    public Course buscarCurso(String codigo) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<CourseGrid> buscarTodosDisciplina() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Course buscarDisciplina(String codigo) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Department buscarDepartamento(String codigo) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Version buscarVersionCodigo(String codigo) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<Teacher> listarProfessores() throws RemoteException {
        return daoTeacher.listarTodos();
    }
    
    @Override
    public List<Teacher> buscarTodosProfessores() throws  RemoteException{
        return daoTeacher.listarTodos();
    }
    
    @Override
    public boolean atualizarProfessor(Teacher teacher) throws RemoteException {
        return daoTeacher.atualizar(teacher);
    }

    @Override
    public boolean removerProfessor(Teacher teacher) throws RemoteException {
        return daoTeacher.delete(teacher);
    }

    @Override
    public boolean atualizarDisponibilidade(TeacherAvailability teacherAvailability) throws RemoteException {
        return daoTeacherAvailability.atualizar(teacherAvailability);
    }

    @Override
    public boolean removerDisponibilidade(TeacherAvailability teacherAvailability) throws RemoteException {
        return daoTeacherAvailability.delete(teacherAvailability);
    }

    @Override
    public List<TeacherAvailability> listarDisponibilidadesProfessor(String codigo) throws RemoteException {
        return daoTeacherAvailability.listarDisponibilidade(codigo);
    }
    
    /*Atualizar e Remover Curso*/
    @Override
    public boolean atualizarCurso(Course course) throws RemoteException{
        return daoCourse.update(course);
    }
    
    @Override
    public boolean removerCurso(Course course) throws RemoteException{
        return daoCourse.delete(course);
    }
}
