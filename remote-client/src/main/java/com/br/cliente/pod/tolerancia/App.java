package com.br.cliente.pod.tolerancia;

import com.br.interfaces.beans.Disponibilidade;
import com.br.interfaces.beans.Nivel;
import com.br.interfaces.beans.Professor;
import com.br.interfaces.beans.Regime;
import com.br.interfaces.beans.Semestre;
import com.br.interfaces.beans.Turno;
import com.br.interfaces.beans.Unidade;
import com.br.interfaces.beans.Vinculo;
import com.br.interfaces.pod.InterfaceFachadaMySQL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {

        InterfaceFachadaMySQL fachada;

        try {

            Registry registry = LocateRegistry.getRegistry("localhost", 10909);
            fachada = (InterfaceFachadaMySQL) registry.lookup("fachadaJNDI");

            System.out.println(fachada.existeProfessor("12"));
            
        } catch (RemoteException | NotBoundException ex) {
            ex.printStackTrace();
        }


    }
}
