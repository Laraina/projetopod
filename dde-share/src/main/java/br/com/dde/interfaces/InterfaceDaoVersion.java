package br.com.dde.interfaces;

import br.com.dde.entidades.Version;

public interface InterfaceDaoVersion {
	
	public void salvar(Version version);
	
	public void atualizar(Version version);
	
	public Version buscar(String code);
	
	public void delete(Version version);

}
