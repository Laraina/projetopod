package br.com.dde.interfaces;

import br.com.dde.entidades.Department;

public interface InterfaceDaoDepartment {
	
	public void salvar(Department department);
	
	public void atualizar(Department department);
	
	public Department buscar(String code);
	
	public void delete(Department department);

}
