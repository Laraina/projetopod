package br.com.dde.interfaces;

import br.com.dde.entidades.Course;
import java.util.List;

public interface InterfaceDaoCourse {

	public boolean salvar(Course cource);

	public boolean update(Course course);

	public Course buscar(String code);

	public boolean delete(Course appUser);
        
        public List<Course> buscarTodosCurso();

}
