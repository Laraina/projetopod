package br.com.dde.interfaces;

import br.com.dde.entidades.AppUser;

public interface InterfaceDaoAppUser {
	
	public void salvar(AppUser appUser);
	
	public void atualizar(AppUser appUser);
	
	public AppUser buscar(String code); 
	
	public void delete(AppUser appUser);

}
