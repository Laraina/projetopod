package br.com.dde.interfaces;

import br.com.dde.entidades.TeacherCourseGrid;

public interface InterfaceDaoTeacherCourseGrid {
	
	public void salvar(TeacherCourseGrid teacherCourseGrid);
	
	public void atualizar(TeacherCourseGrid teacherCourseGrid);
	
	public TeacherCourseGrid buscar(String code);
	
	public void delete(TeacherCourseGrid teacherCourseGrid);

}
