package br.com.dde.interfaces;

import br.com.dde.entidades.Teacher;
import java.util.List;

public interface InterfaceDaoTeacher {
	
	public void salvar(Teacher teacher);
	
	public boolean atualizar(Teacher teacher);
	
	public Teacher buscar(String code);
	
	public boolean delete(Teacher teacher);
        
        public List<Teacher> listarTodos();

}
