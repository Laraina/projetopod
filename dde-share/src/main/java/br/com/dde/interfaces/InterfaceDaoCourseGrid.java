package br.com.dde.interfaces;

import br.com.dde.entidades.CourseGrid;

public interface InterfaceDaoCourseGrid {
	
	public void salvar (CourseGrid courseGrid);
	
	public void atualizar(CourseGrid courseGrid);
	
	public CourseGrid buscar(String code);
	
	public void delete(CourseGrid courseGrid);

}
