package br.com.dde.interfaces;

import br.com.dde.entidades.TeacherUser;

public interface InterfaceDaoTeacherUser {
	
	public void salvar(TeacherUser teacherUser);
	
	public void atualizar(TeacherUser teacherUser);
	
	public TeacherUser buscar(String code, String usuario);
	
	public void delete(TeacherUser teacherUser);

}
