package br.com.dde.interfaces;

import br.com.dde.entidades.TeacherAvailability;
import java.util.List;

public interface InterfaceDaoTeacherAvailability {
	
	public void salvar(TeacherAvailability teacherAvailability);
	
	public boolean atualizar(TeacherAvailability teacherAvailability);
	
	public TeacherAvailability buscar(String code);
	
	public boolean delete(TeacherAvailability teacherAvailability);
        
        public List<TeacherAvailability> listarDisponibilidade(String code);
        
        public TeacherAvailability ultimaDispProfessor(String teacher, String version);

}
