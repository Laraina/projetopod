package br.com.dde.entidades;

import java.io.Serializable;
import java.math.BigInteger;

public class NotificationEvent implements Serializable {

	private String code;
	private String hashCode;
	private String author;
	private BigInteger created;
	private String message;
	private String error;
	private String status;

	public NotificationEvent() {
		// TODO Auto-generated constructor stub
	}

	public NotificationEvent(String code, String hashCode, String author,
			BigInteger created, String message, String error, String status) {
		super();
		this.code = code;
		this.hashCode = hashCode;
		this.author = author;
		this.created = created;
		this.message = message;
		this.error = error;
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public BigInteger getCreated() {
		return created;
	}

	public void setCreated(BigInteger created) {
		this.created = created;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
