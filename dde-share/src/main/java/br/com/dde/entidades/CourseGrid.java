package br.com.dde.entidades;

import java.io.Serializable;

public class CourseGrid implements Serializable {

	private String code;
	private Course course;
	private int unitPart;
	private String unitName;
	private int unitCredits;
	private int unitHours;
	private String status;

	public CourseGrid() {
		// TODO Auto-generated constructor stub
	}

	public CourseGrid(String code, Course course, int unitPart,
			String unitName, int unitCredits, int unitHours, String status) {
		super();
		this.code = code;
		this.course = course;
		this.unitPart = unitPart;
		this.unitName = unitName;
		this.unitCredits = unitCredits;
		this.unitHours = unitHours;
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public int getUnitPart() {
		return unitPart;
	}

	public void setUnitPart(int unitPart) {
		this.unitPart = unitPart;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public int getUnitCredits() {
		return unitCredits;
	}

	public void setUnitCredits(int unitCredits) {
		this.unitCredits = unitCredits;
	}

	public int getUnitHours() {
		return unitHours;
	}

	public void setUnitHours(int unitHours) {
		this.unitHours = unitHours;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
