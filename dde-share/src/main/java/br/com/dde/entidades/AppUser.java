package br.com.dde.entidades;

import java.io.Serializable;

public class AppUser implements Serializable {

	private String userName;
	private String passKey;
	private String profile;

	public AppUser() {
		// TODO Auto-generated constructor stub
	}

	public AppUser(String userName, String passKey, String profile) {
		super();
		this.userName = userName;
		this.passKey = passKey;
		this.profile = profile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassKey() {
		return passKey;
	}

	public void setPassKey(String passKey) {
		this.passKey = passKey;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

}
