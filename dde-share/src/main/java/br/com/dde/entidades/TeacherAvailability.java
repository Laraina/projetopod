package br.com.dde.entidades;

import java.io.Serializable;

public class TeacherAvailability implements Serializable {

    private int flagMonday;
    private int flagTuesday;
    private int flagWednesday;
    private int flagThursday;
    private int flagFriday;
    private int flagPhding;
    private int flagMsing;
    private Version version;
    private Teacher teacher;

    public TeacherAvailability() {
        // TODO Auto-generated constructor stub
    }

    public TeacherAvailability(int flagMonday, int flagTuesday, int flagWednesday, int flagThursday, int flagFriday, int flagPhding, int flagMsing, Version version, Teacher teacher) {
        this.flagMonday = flagMonday;
        this.flagTuesday = flagTuesday;
        this.flagWednesday = flagWednesday;
        this.flagThursday = flagThursday;
        this.flagFriday = flagFriday;
        this.flagPhding = flagPhding;
        this.flagMsing = flagMsing;
        this.version = version;
        this.teacher = teacher;
    }

    public int getFlagMonday() {
        return flagMonday;
    }

    public void setFlagMonday(int flagMonday) {
        this.flagMonday = flagMonday;
    }

    public int getFlagTuesday() {
        return flagTuesday;
    }

    public void setFlagTuesday(int flagTuesday) {
        this.flagTuesday = flagTuesday;
    }

    public int getFlagWednesday() {
        return flagWednesday;
    }

    public void setFlagWednesday(int flagWednesday) {
        this.flagWednesday = flagWednesday;
    }

    public int getFlagThursday() {
        return flagThursday;
    }

    public void setFlagThursday(int flagThursday) {
        this.flagThursday = flagThursday;
    }

    public int getFlagFriday() {
        return flagFriday;
    }

    public void setFlagFriday(int flagFriday) {
        this.flagFriday = flagFriday;
    }

    public int getFlagPhding() {
        return flagPhding;
    }

    public void setFlagPhding(int flagPhding) {
        this.flagPhding = flagPhding;
    }

    public int getFlagMsing() {
        return flagMsing;
    }

    public void setFlagMsing(int flagMsing) {
        this.flagMsing = flagMsing;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

}
