package br.com.dde.entidades;

import java.io.Serializable;

public class Teacher implements Serializable {

	private String code;
	private String name;
	private String abreviation;
	private String email;
	private Department department;
	private String phone;
	private String status;

	public Teacher() {
		// TODO Auto-generated constructor stub
	}

	public Teacher(String code, String name, String abreviation, String email,
			Department department, String phone, String status) {
		super();
		this.code = code;
		this.name = name;
		this.abreviation = abreviation;
		this.email = email;
		this.department = department;
		this.phone = phone;
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbreviation() {
		return abreviation;
	}

	public void setAbreviation(String abreviation) {
		this.abreviation = abreviation;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
