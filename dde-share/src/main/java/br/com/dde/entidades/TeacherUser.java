package br.com.dde.entidades;

import java.io.Serializable;

public class TeacherUser implements Serializable {

	private Teacher teacher;
	private AppUser appUser;

	public TeacherUser() {
		// TODO Auto-generated constructor stub
	}

	public TeacherUser(Teacher teacher, AppUser appUser) {
		super();
		this.teacher = teacher;
		this.appUser = appUser;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public AppUser getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

}
