package br.com.dde.entidades;

import java.io.Serializable;

public class TeacherCourseGrid implements Serializable {

	private Version version;
	private CourseGrid courseGrid;
	private Teacher teacher;

	public TeacherCourseGrid() {
		// TODO Auto-generated constructor stub
	}

	public TeacherCourseGrid(Version version, CourseGrid courseGrid,
			Teacher teacher) {
		super();
		this.version = version;
		this.courseGrid = courseGrid;
		this.teacher = teacher;
	}

	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		this.version = version;
	}

	public CourseGrid getCourseGrid() {
		return courseGrid;
	}

	public void setCourseGrid(CourseGrid courseGrid) {
		this.courseGrid = courseGrid;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
