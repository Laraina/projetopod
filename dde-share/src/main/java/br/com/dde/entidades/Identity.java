package br.com.dde.entidades;

import java.io.Serializable;

public class Identity implements Serializable {

	private String symbol;
	private int value;

	public Identity() {
		// TODO Auto-generated constructor stub
	}

	public Identity(String symbol, int value) {
		super();
		this.symbol = symbol;
		this.value = value;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
