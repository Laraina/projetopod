package br.com.dde.entidades;

import java.io.Serializable;

public class Department implements Serializable {

	private String code;
	private String name;

	public Department() {
		// TODO Auto-generated constructor stub
	}

	public Department(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
