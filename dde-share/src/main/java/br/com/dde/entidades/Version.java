package br.com.dde.entidades;

import java.io.Serializable;
import java.math.BigInteger;

public class Version implements Serializable {

    private String code;
    private int year;
    private int part;
    private int flagDefault;
    private BigInteger created;

    public Version() {
        // TODO Auto-generated constructor stub
    }

    public Version(String code, int year, int part, char flagDefault,
            BigInteger created) {
        super();
        this.code = code;
        this.year = year;
        this.part = part;
        this.flagDefault = flagDefault;
        this.created = created;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public int getFlagDefault() {
        return flagDefault;
    }

    public void setFlagDefault(int flagDefault) {
        this.flagDefault = flagDefault;
    }

    public BigInteger getCreated() {
        return created;
    }

    public void setCreated(BigInteger created) {
        this.created = created;
    }

}
