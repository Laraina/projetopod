package br.com.dde.fachada;

import br.com.dde.entidades.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface InterfaceFachada extends Remote{

    public boolean salvarCourseGrid(CourseGrid course) throws RemoteException;
    
    public boolean salvarAppUser(AppUser user) throws RemoteException;
    
    public boolean salvarCourse(Course course) throws RemoteException;
    
    public boolean salvarDepartment(Department dep) throws RemoteException;
    
    public boolean salvarTeacher(Teacher t) throws RemoteException;
    
    public boolean salvarTeacherAvailability(TeacherAvailability teacherAvailability) throws RemoteException;
    
    public boolean salvarTeacherCourseGrid(TeacherCourseGrid teacherCourseGrid) throws RemoteException;
    
    public boolean salvarTeacherUser(TeacherUser teacherUser) throws RemoteException ;
    
    public boolean salvarVersion(Version version) throws RemoteException;
        
    public String mensagemConfirmacao() throws RemoteException;
    
    /*Consultas*/
    
    public List<Teacher> buscarTodosProfessores() throws  RemoteException;
    
    public Teacher buscarProfessor(String codigo) throws RemoteException;
    
    public List<Course> buscarTodosCurso() throws RemoteException;
    
    public Course buscarCurso(String indice) throws RemoteException;
    
    public List<CourseGrid> buscarTodosDisciplina() throws RemoteException;
    
    public Course buscarDisciplina(String indice) throws RemoteException;
    
    public Department buscarDepartamento(String codigo) throws RemoteException;
    
    public Version buscarVersionCodigo(String codigo) throws RemoteException;
    
    public List<Teacher> listarProfessores() throws RemoteException;
    
    /*Metodos de Atualizar e Remover Professor e Disponibilidade*/
    public boolean atualizarProfessor(Teacher teacher) throws RemoteException;
    
    public boolean removerProfessor(Teacher teacher) throws RemoteException;
   
    public boolean atualizarDisponibilidade(TeacherAvailability teacherAvailability) throws RemoteException;
    
    public boolean removerDisponibilidade(TeacherAvailability teacherAvailability) throws RemoteException;
    
    public List<TeacherAvailability> listarDisponibilidadesProfessor(String codigo) throws RemoteException;

    /*Remover e Atualizar Curso*/
    public boolean atualizarCurso(Course course) throws RemoteException;
    
    public boolean removerCurso(Course course) throws RemoteException;
}
